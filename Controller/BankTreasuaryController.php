<?php
namespace Terminalbd\NbrvatBundle\Controller;

use App\Entity\Application\Nbrvat;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Terminalbd\AccountingBundle\Repository\AccountBankRepository;
use Terminalbd\NbrvatBundle\Entity\BankTreasury;
use Terminalbd\NbrvatBundle\Form\TreasuaryFormType;

/**
 * @Route("/nbrvat/bank-treasuary")
 * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT')")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class BankTreasuaryController extends AbstractController
{
    /**
     * @Route("/", methods={"GET"}, name="nbrvat_treasuary")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT_SETTING')")
     */
    public function index(Request $request): ? Response
    {
        $config = $this->getDoctrine()->getRepository(Nbrvat::class)->findConfig($this->getUser());
        $posts = $this->getDoctrine()->getRepository('TerminalbdNbrvatBundle:BankTreasury')->findBy(array('config' => $config));
        return $this->render('@TerminalbdNbrvat/treasuary/index.html.twig', [
            'entities' => $posts
        ]);
    }

    /**
     * @Security("is_granted('ROLE_DOMAIN') or (is_granted('ROLE_NBRVAT_SETTING') and is_granted('ROLE_NBRVAT_CREATE'))")
     * @Route("/new", methods={"GET", "POST"}, name="nbrvat_treasuary_new")
     */
    public function new(Request $request): Response
    {
        $config = $this->getDoctrine()->getRepository(Nbrvat::class)->findConfig($this->getUser());
        $post = new BankTreasury();
        $form = $this->createForm(TreasuaryFormType::class , $post)
            ->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $post->setConfig($config);
            $em->persist($post);
            $em->flush();
            $this->addFlash('success', 'post.created_successfully');
            if ($form->get('SaveAndCreate')->isClicked()) {
                return $this->redirectToRoute('nbrvat_treasuary_new');
            }
            return $this->redirectToRoute('nbrvat_treasuary_new');
        }
        return $this->render('@TerminalbdNbrvat/treasuary/new.html.twig', [
            'post' => $post,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/edit",methods={"GET", "POST"}, name="nbrvat_treasuary_edit")
     * @Security("is_granted('ROLE_DOMAIN') or (is_granted('ROLE_NBRVAT_SETTING') and is_granted('ROLE_NBRVAT_EDIT'))")
     */
    public function edit(Request $request,BankTreasury $post): Response
    {

        $form = $this->createForm(TreasuaryFormType::class, $post)
            ->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'post.updated_successfully');
            if ($form->get('SaveAndCreate')->isClicked()) {
                return $this->redirectToRoute('nbrvat_treasuary_edit', ['id' => $post->getId()]);
            }
            return $this->redirectToRoute('nbrvat_treasuary');
        }
        return $this->render('TerminalbdNbrvatBundle:treasuary:new.html.twig', [
            'post' => $post,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Deletes a AccountBank entity.
     *
     * @Route("/{id}/delete",methods={"GET", "POST"}, name="nbrvat_treasuary_delete")
     * @Security("is_granted('ROLE_DOMAIN') or (is_granted('ROLE_NBRVAT_SETTING') and is_granted('ROLE_NBRVAT_DELETE'))")
     */
    public function delete($id): Response
    {
        $post = $this->getDoctrine()->getRepository(BankTreasury::class)->findOneBy(['id'=> $id]);
        $em = $this->getDoctrine()->getManager();
        $em->remove($post);
        $em->flush();
        $this->addFlash('success', 'post.deleted_successfully');
        return new Response('Success');
    }

    /**
     * Process a Porduction Issue entity.
     *
     * @Route("/{id}/process", methods={"GET"}, name="nbrvat_treasuary_process")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT')")
     */
    public function process($id): Response
    {
        $entity = $this->getDoctrine()->getRepository(BankTreasury::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        $entity->setApprovedBy($this->getUser());
        $entity->setProcess('approved');
        $em->flush();
        return new Response('Success');

    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{mode}/process-group", methods={"GET"}, name="nbrvat_treasuary_process_group")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_ISSUE')")
     */
    public function processGroup($mode): Response
    {
        $em = $this->getDoctrine()->getManager();
        $ids = explode(',',$_REQUEST['ids']);
        foreach ($ids as $key => $id):
            $post = $this->getDoctrine()->getRepository("TerminalbdNbrvatBundle:BankTreasury")->find($id);
            $entity->setApprovedBy($this->getUser());
            $entity->setProcess('approved');
            $em->flush();
        endforeach;
        return new Response('Success');

    }

}
