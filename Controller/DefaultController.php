<?php
/**
 * Created by PhpStorm.
 * User: shafiq
 * Date: 9/29/19
 * Time: 9:09 PM
 */

namespace Terminalbd\NbrvatBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{


    /**
     * @Route("nbrvat", name="vat_index")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    function index() {
        exit;
        return $this->render('@TerminalbdNbrvatBundle/post/index.html.twig');
    }

}