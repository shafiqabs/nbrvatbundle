<?php

namespace Terminalbd\NbrvatBundle\Controller;
use App\Entity\Application\Nbrvat;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\NbrvatBundle\Entity\NetTaxPayable;
use Terminalbd\NbrvatBundle\Entity\VatLedger;
use Terminalbd\NbrvatBundle\Entity\VatOldLedger;
use Terminalbd\NbrvatBundle\Form\TaxPayableFormType;


/**
 * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT_taxpayable')")
 * @Route("/nbrvat/tax-payable")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class NetTaxPayableController extends AbstractController
{
    /**
     * @Route("/", methods={"GET"}, name="nbrvat_taxpayable")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT_SETTING')")
     */
    public function index(Request $request): Response
    {
        $entities = $this->getDoctrine()->getRepository(NetTaxPayable::class)->findAll();
        return $this->render('@TerminalbdNbrvat/taxpayable/index.html.twig', [
            'entities' => $entities,
        ]);
    }

    /**
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT_SETTING')")
     * @Route("/new", methods={"GET", "POST"}, name="nbrvat_taxpayable_new")
     */
    public function new(Request $request): Response
    {

        $entity = new NetTaxPayable();
        $data = $request->request->all();
        $form = $this->createForm(TaxPayableFormType::class , $entity)
            ->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);
        $config = $this->getDoctrine()->getRepository(Nbrvat::class)->findConfig($this->getUser());
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity->setConfig($config);
            $em->persist($entity);
            $em->flush();
            $this->addFlash('success', 'post.created_successfully');
            if ($form->get('SaveAndCreate')->isClicked()) {
                return $this->redirectToRoute('nbrvat_taxpayable_new');
            }
            return $this->redirectToRoute('nbrvat_taxpayable');
        }
        return $this->render('@TerminalbdNbrvat/taxpayable/new.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/edit", methods={"GET", "POST"}, name="nbrvat_taxpayable_edit")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT_SETTING')")
     */
    public function edit(Request $request, NetTaxPayable $entity): Response
    {
        $data = $request->request->all();
        $form = $this->createForm(TaxPayableFormType::class, $entity)
            ->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'post.updated_successfully');
            if ($form->get('SaveAndCreate')->isClicked()) {
                return $this->redirectToRoute('nbrvat_taxpayable_edit', ['id' => $entity->getId()]);
            }

            return $this->redirectToRoute('nbrvat_taxpayable');
        }
        return $this->render('@TerminalbdNbrvat/taxpayable/new.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/delete", methods={"GET"}, name="nbrvat_taxpayable_delete")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT_SETTING')")
     */

    public function delete($id): Response
    {
        $entity = $this->getDoctrine()->getRepository(Setting::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($entity);
        $em->flush();
        $this->addFlash('success', 'post.deleted_successfully');
        return new Response('success');
    }

    /**
     * Process a Porduction Issue entity.
     *
     * @Route("/{id}/process", methods={"GET"}, name="nbrvat_taxpayable_process")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT')")
     */

    public function process($id): Response
    {
        $entity = $this->getDoctrine()->getRepository(NetTaxPayable::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        $entity->setApprovedBy($this->getUser());
        $entity->setProcess('approved');
        $em->flush();
        $arrs = array('part-7-closing-vat','part-7-closing-sd');
        if($entity->getTaxPayable()->getSlug() == "part-7-closing-vat"){
            $this->getDoctrine()->getRepository(VatLedger::class)->insertVAT($entity);
        }
        if($entity->getTaxPayable()->getSlug() == "part-7-closing-sd"){
            $this->getDoctrine()->getRepository(VatLedger::class)->insertSD($entity);
        }
         if($entity->getTaxPayable()->getSlug() == "part-8-remaining-balance-vat"){
            $this->getDoctrine()->getRepository(VatOldLedger::class)->insertVAT($entity);
        }
        if($entity->getTaxPayable()->getSlug() == "part-8-remaining-balance-sd"){
            $this->getDoctrine()->getRepository(VatOldLedger::class)->insertSD($entity);
        }
        return new Response('success');

    }

}
