<?php
namespace Terminalbd\NbrvatBundle\Controller;

use App\Entity\Admin\SettingType;
use App\Entity\Admin\Terminal;
use App\Entity\Application\Inventory;
use App\Entity\Application\Nbrvat;
use App\Entity\Application\Production;
use App\Service\ConfigureManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Terminalbd\AccountingBundle\Repository\AccountBankRepository;
use Terminalbd\InventoryBundle\Entity\BranchIssue;
use Terminalbd\InventoryBundle\Entity\Purchase;
use Terminalbd\InventoryBundle\Entity\PurchaseReturn;
use Terminalbd\InventoryBundle\Entity\PurchaseReturnItem;
use Terminalbd\InventoryBundle\Entity\Sales;
use Terminalbd\InventoryBundle\Entity\SalesReturn;
use Terminalbd\InventoryBundle\Entity\StockItem;
use Terminalbd\NbrvatBundle\Entity\BankTreasury;
use Terminalbd\NbrvatBundle\Entity\Setting;
use Terminalbd\NbrvatBundle\Entity\TaxReturn;
use Terminalbd\NbrvatBundle\Entity\VdsCertificate;
use Terminalbd\NbrvatBundle\Entity\VdsTransaction;
use Terminalbd\NbrvatBundle\Form\TreasuaryFormType;
use Terminalbd\ProductionBundle\Entity\ProductionBatch;
use Terminalbd\ProductionBundle\Entity\ProductionItem;

/**
 * @Route("/nbrvat/report")
 * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT')")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class ReportController extends AbstractController
{
    /**
     * @Route("/mushuk/4.3", methods={"GET"}, name="nbrvat_report_mushuk_4_3")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT_REPORT')")
     */
    public function mushuk43(Request $request): ? Response
    {
        $data = $_REQUEST;
        $config = $this->getDoctrine()->getRepository(Production::class)->findConfig($this->getUser());
        $products = $this->getDoctrine()->getRepository("TerminalbdProductionBundle:ProductionItem")->findBy(array('config'=>$config));
        if(isset($data['item']) and !empty($data['item'])){
            $productionItem = $this->getDoctrine()->getRepository('TerminalbdProductionBundle:ProductionItem')->find($data['item']);
        }else{
            $productionItem = "";
        }
        return $this->render('@TerminalbdNbrvat/report/mushok43/index.html.twig', [
            'entities' => $products,
            'productionItem' => $productionItem,
            'searchForm' => $data
        ]);
    }


    /**
    
     *
     * @Route("/{id}/mushuk/4.3/preview", methods={"GET"}, name="nbrvat_report_mushuk_4_3_preview")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT_REPORT')")
     */

    public function mushuk43Preview(Request $request, ProductionItem $entity): Response
    {

        /* @var $domain Terminal */
        $domain =$this->getUser()->getTerminal();

        $arrElemetns = array();
        $arrValues = array();
        $i = 0;
        foreach ($entity->getElements() as $key => $element){
            $ele = "element-{$key}";
            $arrElemetns[$ele] = $element;
        }
        foreach ($entity->getProductionValueAddeds() as $key => $valueAdded){
            $ele = "value-{$key}";
            $arrValues[$ele] = $valueAdded;
        }
        return $this->render('@TerminalbdNbrvat/report/mushok43/mushok.html.twig', [
            'domain' => $domain,
            'item' => $entity,
            'arrElemetns' => $arrElemetns,
            'arrValues' => $arrValues,
            'mode' => 'print',
        ]);
    }


    /**
     * @Route("{id}/{mode}/mushuk/4.3/download", methods={"GET"}, name="nbrvat_report_mushuk_4_3_print")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT_REPORT')")
     */

    public function mushuk43Print(Request $request,ProductionItem $entity): Response
    {
        $data = $_REQUEST;
        /* @var $domain Terminal */
        $domain =$this->getUser()->getTerminal();
        foreach ($entity->getElements() as $key => $element){
            $ele = "element-{$key}";
            $arrElemetns[$ele] = $element;
        }
        foreach ($entity->getProductionValueAddeds() as $key => $valueAdded){
            $ele = "value-{$key}";
            $arrValues[$ele] = $valueAdded;
        }
        return $this->render('@TerminalbdNbrvat/report/mushok43/mushok.html.twig', [
            'domain' => $domain,
            'item' => $entity,
            'arrElemetns' => $arrElemetns,
            'arrValues' => $arrValues,
            'mode' => 'print',
        ]);

    }


    /**
     * @Route("/mushuk/6.1", methods={"GET"}, name="nbrvat_report_mushuk_6_1")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT_SETTING')")
     */
    public function mushuk61(Request $request): ? Response
    {
        $data = $_REQUEST;
        $item = '';
        $config = $this->getDoctrine()->getRepository(Inventory::class)->findConfig($this->getUser());
        $modes = array("raw-material","consumable","stockable","assembled","finish-goods");
        $products = $this->getDoctrine()->getRepository("TerminalbdInventoryBundle:Item")->modeWiseStockItem($config,$modes);
        if(isset($data['item'])){
            $item = $data['item'];
            $item = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:Item')->find($item);
        }
        return $this->render('@TerminalbdNbrvat/report/mushok61/index.html.twig', [
            'entities' => $products,
            'product' => $item,
            'searchForm' => $data
        ]);
    }


    /**
    
     *
     * @Route("/mushuk/6.1/preview", methods={"GET"}, name="nbrvat_report_mushuk_6_1_preview")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT_REPORT')")
     */

    public function mushuk61Preview(Request $request): Response
    {

        $data = $_REQUEST;
        $product = $data['id'];
        $item = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:Item')->find($product);
        $entities = $this->getDoctrine()->getRepository(StockItem::class)->getPurchaseLadger($product);
        /* @var $domain Terminal */
        $domain =$this->getUser()->getTerminal();
        return $this->render('@TerminalbdNbrvat/report/mushok61/mushok.html.twig', [
            'domain' => $domain,
            'entities' => $entities,
            'item' => $item,
            'mode' => 'print',
        ]);
    }


    /**
    
     *
     * @Route("{mode}/mushuk/6.1/download", methods={"GET"}, name="nbrvat_report_mushuk_6_1_print")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT_REPORT')")
     */

    public function mushuk61Print(Request $request): Response
    {
        $data = $_REQUEST;
        $product = $data['id'];
        $item = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:Item')->find($product);
        $entities = $this->getDoctrine()->getRepository(StockItem::class)->getPurchaseLadger($product);
        /* @var $domain Terminal */
        $domain =$this->getUser()->getTerminal();
        return $this->render('@TerminalbdNbrvat/report/mushok61/mushok.html.twig', [
            'domain' => $domain,
            'entities' => $entities,
            'item' => $item,
            'mode' => 'print',
        ]);


    }

    /**
     * @Route("/mushuk/6.2", methods={"GET"}, name="nbrvat_report_mushuk_6_2")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT_SETTING')")
     */
    public function mushuk62(Request $request): ? Response
    {
        $data = $_REQUEST;
        $item = '';
        $config = $this->getDoctrine()->getRepository(Inventory::class)->findConfig($this->getUser());
        $modes = array("production-item","finish-goods");
        $products = $this->getDoctrine()->getRepository("TerminalbdInventoryBundle:Item")->modeWiseStockItem($config,$modes);
        if(isset($data['item'])){
            $item = $data['item'];
            $item = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:Item')->find($item);
        }
        return $this->render('@TerminalbdNbrvat/report/mushok62/index.html.twig', [
            'entities' => $products,
            'product' => $item,
            'searchForm' => $data
        ]);
    }


    /**
    
     *
     * @Route("/mushuk/6.2/preview", methods={"GET"}, name="nbrvat_report_mushuk_6_2_preview")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT_REPORT')")
     */

    public function mushuk62Preview(Request $request): Response
    {

        $data = $_REQUEST;
        $product = $data['id'];
        $item = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:Item')->find($product);
        $entities = $this->getDoctrine()->getRepository(StockItem::class)->getPurchaseLadger($product);

        /* @var $domain Terminal */
        $domain =$this->getUser()->getTerminal();

        return $this->render('@TerminalbdNbrvat/report/mushok62/mushok.html.twig', [
            'domain' => $domain,
            'entities' => $entities,
            'item' => $item,
            'mode' => 'print',
        ]);
    }


    /**
    
     *
     * @Route("{mode}/mushuk/6.2/download", methods={"GET"}, name="nbrvat_report_mushuk_6_2_print")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT_REPORT')")
     */

    public function mushuk62Print(Request $request): Response
    {
        $data = $_REQUEST;
        $product = $data['id'];
        $item = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:Item')->find($product);
        $entities = $this->getDoctrine()->getRepository(StockItem::class)->getPurchaseLadger($product);
        /* @var $domain Terminal */
        $domain =$this->getUser()->getTerminal();
        return $this->render('@TerminalbdNbrvat/report/mushok62/mushok.html.twig', [
            'domain' => $domain,
            'entities' => $entities,
            'item' => $item,
            'mode' => 'print',
        ]);


    }


    /**
     * @Route("/mushuk/6.2.1", methods={"GET"}, name="nbrvat_report_mushuk_6_2_1")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT_SETTING')")
     */
    public function mushuk621(Request $request): ? Response
    {
        $data = $_REQUEST;
        $item = '';
        $config = $this->getDoctrine()->getRepository(Inventory::class)->findConfig($this->getUser());
        $modes = array("production-item","finish-goods");
        $products = $this->getDoctrine()->getRepository("TerminalbdInventoryBundle:Item")->modeWiseStockItem($config,$modes);
        if(isset($data['item'])){
            $item = $data['item'];
            $item = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:Item')->find($item);
        }
        return $this->render('@TerminalbdNbrvat/report/mushok621/index.html.twig', [
            'entities' => $products,
            'product' => $item,
            'searchForm' => $data
        ]);
    }


    /**

     *
     * @Route("{mode}/mushuk/6.2.1/report", methods={"GET"}, name="nbrvat_report_mushuk_6_2_1_preview")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT_REPORT')")
     */

    public function mushuk621Print(Request $request): Response
    {
        $data = $_REQUEST;
        $product = $data['id'];
        $item = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:Item')->find($product);
        $entities = $this->getDoctrine()->getRepository(StockItem::class)->getPurchaseLadger($product);
        /* @var $domain Terminal */
        $domain =$this->getUser()->getTerminal();
        return $this->render('@TerminalbdNbrvat/report/mushok621/mushok.html.twig', [
            'domain' => $domain,
            'entities' => $entities,
            'item' => $item,
            'mode' => 'print',
        ]);


    }

    /**
     * @Route("/mushuk/6.4", methods={"GET"}, name="nbrvat_report_mushuk_6_4")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT_SETTING')")
     */
    public function mushuk64(Request $request): ? Response
    {
        $data = $_REQUEST;
        $item = '';
        $config = $this->getDoctrine()->getRepository(Inventory::class)->findConfig($this->getUser());
        $batchs = $this->getDoctrine()->getRepository(ProductionBatch::class)->findBy(array('mode'=>'contractual'));
        if(isset($data['item'])){
            $item = $data['item'];
            $item = $this->getDoctrine()->getRepository('TerminalbdProductionBundle:ProductionBatch')->find($item);
        }

        return $this->render('@TerminalbdNbrvat/report/mushok64/index.html.twig', [
            'entities' => $batchs,
            'searchForm' => $data
        ]);
    }


    /**

     *
     * @Route("/{mode}/mushuk/6.4/download", methods={"GET"}, name="nbrvat_report_mushuk_6_4_print")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT_REPORT')")
     */

    public function mushuk64Print(Request $request): Response
    {
        $data = $_REQUEST;
        $product = $data['id'];
        $item = $this->getDoctrine()->getRepository('TerminalbdProductionBundle:ProductionBatch')->find($product);
        /* @var $domain Terminal */
        $domain =$this->getUser()->getTerminal();
        return $this->render('@TerminalbdNbrvat/report/mushok64/mushok.html.twig', [
            'domain' => $domain,
            'entity' => $item,
            'mode' => 'print',
        ]);


    }



    /**
     * @Route("/mushuk/6.5", methods={"GET"}, name="nbrvat_report_mushuk_6_5")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT_SETTING')")
     */
    public function mushuk65(Request $request): ? Response
    {
        $data = $_REQUEST;
        $config = $this->getDoctrine()->getRepository(Inventory::class)->findConfig($this->getUser());
        $entities = $this->getDoctrine()->getRepository("TerminalbdInventoryBundle:BranchIssue")->findBy(array('config' => $config));
        if(isset($data['item']) and !empty($data['item'])){
            $post = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:BranchIssue')->find($data['item']);
        }else{
            $post = "";
        }
        return $this->render('@TerminalbdNbrvat/report/mushok65/index.html.twig', [
            'entities' => $entities,
            'post' => $post,
            'searchForm' => $data
        ]);
    }


    /**
    
     *
     * @Route("{id}/mushuk/6.5/preview", methods={"GET"}, name="nbrvat_report_mushuk_6_5_preview")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT_REPORT')")
     */

    public function mushuk65Preview(Request $request, BranchIssue $post): Response
    {

        /* @var $domain Terminal */
        $domain =$this->getUser()->getTerminal();
        return $this->render('@TerminalbdNbrvat/report/mushok65/mushok.html.twig', [
            'domain' => $domain,
            'post' => $post,
        ]);
    }


    /**
    
     *
     * @Route("/{id}/mushuk/6.5/download", methods={"GET"}, name="nbrvat_report_mushuk_6_5_print")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT_REPORT')")
     */

    public function mushuk65Print(Request $request, BranchIssue $post): Response
    {
        $domain =$this->getUser()->getTerminal();
        return $this->render('@TerminalbdNbrvat/report/mushok65/mushok.html.twig', [
            'domain' => $domain,
            'post' => $post,
        ]);

    }



    /**
     * @Route("/mushuk/6.3", methods={"GET"}, name="nbrvat_report_mushuk_6_3")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT_SETTING')")
     */
    public function mushuk63(Request $request): ? Response
    {
        $data = $_REQUEST;
        $config = $this->getDoctrine()->getRepository(Inventory::class)->findConfig($this->getUser());
        $entites = $this->getDoctrine()->getRepository("TerminalbdInventoryBundle:Sales")->findBy(array('config' => $config,'process' => "approved"));
        if(isset($data['invoice']) and !empty($data['invoice'])){
            $post = $this->getDoctrine()->getRepository(Sales::class)->find($data['invoice']);
        }else{
            $post = "";
        }
        return $this->render('@TerminalbdNbrvat/report/mushok63/index.html.twig', [
            'entities' => $entites,
            'post' => $post,
            'searchForm' => $data
        ]);
    }


    /**
     * @Route("/{id}/{mode}/mushuk/6.3/print", methods={"GET"}, name="nbrvat_report_mushuk_6_3_print")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT_REPORT')")
     */

    public function mushuk63Print(Request $request, Sales $entity): Response
    {
         /* @var $domain Terminal */

        $domain =$this->getUser()->getTerminal();
        $count = $entity->getDownloadCount();
        $em = $this->getDoctrine()->getManager();
        $sum = $count + 1;
        $entity->setDownloadCount($sum);
        $em->persist($entity);
        $em->flush();
        return $this->render('@TerminalbdNbrvat/report/mushok63/mushok.html.twig', [
            'domain' => $domain,
            'entity' => $entity,
            'mode' => 'print',
        ]);

    }

    /**
     * @Route("/{id}/{mode}/mushuk-purchase/6.3/print", methods={"GET"}, name="nbrvat_report_mushuk_purchase_6_3_print")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT_REPORT')")
     */

    public function mushuk63PurchasePrint(Request $request, Purchase $entity): Response
    {
        /* @var $domain Terminal */

        $domain =$this->getUser()->getTerminal();
        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();
        return $this->render('@TerminalbdNbrvat/report/mushokPurchase63/mushok.html.twig', [
            'domain' => $domain,
            'entity' => $entity,
            'mode' => 'print',
        ]);

    }


    /**
     * @Route("/mushuk/6.7", methods={"GET"}, name="nbrvat_report_mushuk_6_7")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT_SETTING')")
     */
    public function mushuk67(Request $request): ? Response
    {
        $data = $_REQUEST;
        $config = $this->getDoctrine()->getRepository(Inventory::class)->findConfig($this->getUser());
        $entites = $this->getDoctrine()->getRepository("TerminalbdInventoryBundle:SalesReturn")->findBy(array('config' => $config,'process' => "approved"));
        if(isset($data['invoice']) and !empty($data['invoice'])){
            $post = $this->getDoctrine()->getRepository(SalesReturn::class)->find($data['invoice']);
        }else{
            $post = "";
        }
        return $this->render('@TerminalbdNbrvat/report/mushok67/index.html.twig', [
            'entities' => $entites,
            'post' => $post,
            'searchForm' => $data
        ]);
    }


    /**
     * @Route("/{id}/{mode}/mushuk/6.7/preview", methods={"GET"}, name="nbrvat_report_mushuk_6_7_preview")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT_REPORT')")
     */

    public function mushuk67Print(Request $request, SalesReturn $entity): Response
    {
        /* @var $domain Terminal */
        $domain =$this->getUser()->getTerminal();
        return $this->render('@TerminalbdNbrvat/report/mushok67/mushok.html.twig', [
            'domain' => $domain,
            'post' => $entity,
            'mode' => 'print',
        ]);

    }

    /**
     * @Route("/mushuk/6.8", methods={"GET"}, name="nbrvat_report_mushuk_6_8")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT_SETTING')")
     */
    public function mushuk68(Request $request): ? Response
    {
        $data = $_REQUEST;
        $config = $this->getDoctrine()->getRepository(Inventory::class)->findConfig($this->getUser());
        $entites = $this->getDoctrine()->getRepository("TerminalbdInventoryBundle:PurchaseReturn")->findBy(array('config' => $config,'process' => "approved"));
        if(isset($data['invoice']) and !empty($data['invoice'])){
            $post = $this->getDoctrine()->getRepository(PurchaseReturn::class)->find($data['invoice']);
        }else{
            $post = "";
        }
        return $this->render('@TerminalbdNbrvat/report/mushok68/index.html.twig', [
            'entities' => $entites,
            'post' => $post,
            'searchForm' => $data
        ]);
    }


    /**
     * @Route("/{id}/{mode}/mushuk/6.8/preview", methods={"GET"}, name="nbrvat_report_mushuk_6_8_preview")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT_REPORT')")
     */

    public function mushuk68Print(Request $request, PurchaseReturn $entity): Response
    {
        /* @var $domain Terminal */
        $domain =$this->getUser()->getTerminal();
        return $this->render('@TerminalbdNbrvat/report/mushok68/mushok.html.twig', [
            'domain' => $domain,
            'post' => $entity,
            'mode' => 'print',
        ]);

    }

     /**
     * @Route("/mushuk/6.6", methods={"GET"}, name="nbrvat_report_mushuk_6_6")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT_SETTING')")
     */
    public function mushuk66(Request $request): ? Response
    {
        $data = $_REQUEST;
        $config = $this->getDoctrine()->getRepository(Nbrvat::class)->findConfig($this->getUser());
        $entites = $this->getDoctrine()->getRepository(VdsTransaction::class)->findBy(array('config' => $config));
        if(isset($data['invoice']) and !empty($data['invoice'])){
            $post = $this->getDoctrine()->getRepository(VdsCertificate::class)->find($data['invoice']);
        }else{
            $post = "";
        }
        return $this->render('@TerminalbdNbrvat/report/mushok66/index.html.twig', [
            'entities' => $entites,
            'post' => $post,
            'searchForm' => $data
        ]);
    }


    /**
     * @Route("/{id}/{mode}/mushuk/6.6/preview", methods={"GET"}, name="nbrvat_report_mushuk_6_6_preview")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT_REPORT')")
     */

    public function mushuk66Print(Request $request, VdsTransaction $entity): Response
    {
        /* @var $domain Terminal */
        $domain =$this->getUser()->getTerminal();
        return $this->render('@TerminalbdNbrvat/report/mushok66/mushok.html.twig', [
            'domain' => $domain,
            'entity' => $entity,
            'mode' => 'print',
        ]);

    }

    /**
     * @Route("/mushuk/6.10", methods={"GET"}, name="nbrvat_report_mushuk_6_10")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT_SETTING')")
     */
    public function mushuk610(Request $request): ? Response
    {
        $data = $_REQUEST;
        $domain =$this->getUser()->getTerminal();
        $config = $this->getDoctrine()->getRepository(Inventory::class)->findConfig($this->getUser());
        if(isset($data['startDate']) and !empty($data['startDate']) and isset($data['endDate']) and !empty($data['endDate'])){
            $sales = $this->getDoctrine()->getRepository("TerminalbdInventoryBundle:Sales")->findReportSearch($config,$data);
            $purchase = $this->getDoctrine()->getRepository(Purchase::class)->findReportSearch($config,$data);
        }else{
            $sales = "";
            $purchase = "";
        }
        return $this->render('@TerminalbdNbrvat/report/mushok610/index.html.twig', [
            'domain' => $domain,
            'sales' => $sales,
            'purchase' => $purchase,
            'searchForm' => $data
        ]);
    }


    /**
     * @Route("/{mode}/mushuk/6.10/preview", methods={"GET"}, name="nbrvat_report_mushuk_6_10_preview")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT_REPORT')")
     */

    public function mushuk610Print(Request $request,  $mode): Response
    {
        /* @var $domain Terminal */
        $domain =$this->getUser()->getTerminal();
        $data = $_REQUEST;
        $config = $this->getDoctrine()->getRepository(Inventory::class)->findConfig($this->getUser());
        if(isset($data['startDate']) and !empty($data['startDate']) and isset($data['endDate']) and !empty($data['endDate'])){
            $sales = $this->getDoctrine()->getRepository(Sales::class)->findReportSearch($config,$data);
            $purchase = $this->getDoctrine()->getRepository(Purchase::class)->findReportSearch($config,$data);

        }else{

            $sales = "";
            $purchase = "";
        }
        return $this->render('@TerminalbdNbrvat/report/mushok610/mushok.html.twig', [
            'domain' => $domain,
            'sales' => $sales,
            'purchase' => $purchase,
            'searchForm' => $data
        ]);

    }


     /**
     * @Route("/mushuk/9.1", methods={"GET"}, name="nbrvat_report_mushuk_9_1")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT_SETTING')")
     */
    public function mushuk91(Request $request): ? Response
    {
        $data = $_REQUEST;
        $domain =$this->getUser()->getTerminal();
        $config = $this->getDoctrine()->getRepository(Nbrvat::class)->findConfig($this->getUser());
        $entities = $this->getDoctrine()->getRepository(TaxReturn::class)->findBy(array(),array('id'=>
            'DESC'));

        if(isset($data['taxPeriod'])){
            $taxPeriod = $data['taxPeriod'];
            $post = $this->getDoctrine()->getRepository(TaxReturn::class)->findOneBy(array('config' => $config , 'id' => $taxPeriod));
        }else{
            $post = "";
        }
        return $this->render('@TerminalbdNbrvat/report/mushok91/index.html.twig', [
            'domain' => $domain,
            'post' => $post,
            'entities' => $entities,
            'searchForm' => $data
        ]);
    }


    /**
     * @Route("/{id}/mushuk/9.1/preview", methods={"GET"}, name="nbrvat_report_mushuk_9_1_preview")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT_REPORT')")
     */

    public function mushuk91Print(TaxReturn $post): Response
    {
        /* @var $domain Terminal */


        $domain =$this->getUser()->getTerminal();
        $inventory = $this->getDoctrine()->getRepository(Inventory::class)->findConfig($this->getUser());
        $config = $this->getDoctrine()->getRepository(Nbrvat::class)->findConfig($this->getUser());

        $inputTax = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:StockItem')->getInputTax($inventory,$post);

        $totalInputTax = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:StockItem')->getTotalInputTax($inventory,$post);

        $totalDebitNote = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:PurchaseReturn')->getTotalDeditNote($inventory,$post);

        $outputTax = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:StockItem')->getOutputTax($inventory,$post);


        $outputRebateTax = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:StockItem')->getOutputRebateTax($inventory,$post);

        $this->getDoctrine()->getRepository('TerminalbdNbrvatBundle:VatAdjustment')->getVatRefundExport($post,$outputRebateTax);

        $this->getDoctrine()->getRepository('TerminalbdNbrvatBundle:VatAdjustment')->getVatRebateForSales($post,$outputRebateTax);

        $totalOutputTax = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:StockItem')->getTotalOutputTax($inventory,$post);

        $totalCreditNote = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:SalesReturn')->getTotalCreditNote($inventory,$post);

        $totalVds = $this->getDoctrine()->getRepository('TerminalbdNbrvatBundle:VdsTransaction')->getTotalVds($post);

        $totalAdjustment = $this->getDoctrine()->getRepository('TerminalbdNbrvatBundle:VatAdjustment')->getTotalAdjustment($post);

        $decreasingAdjustmentParticular = $this->getDoctrine()->getRepository('TerminalbdNbrvatBundle:VatAdjustment')->getAdjustmentParticular($post,"decreasing-adjutment");

        $increasingAdjustmentParticular = $this->getDoctrine()->getRepository('TerminalbdNbrvatBundle:VatAdjustment')->getAdjustmentParticular($post,"increasing-adjutment");

        $totalAdjustmentBankingChannel = $this->getDoctrine()->getRepository('TerminalbdNbrvatBundle:VatAdjustment')->getTotalAdjustmentBankingChannel($post);

        $netTaxPayable = $this->getDoctrine()->getRepository('TerminalbdNbrvatBundle:NetTaxPayable')->getNetTaxPayableSummary($post);

        $bankTreasury = $this->getDoctrine()->getRepository('TerminalbdNbrvatBundle:BankTreasury')->getBankTreasurySummary($post);

        $totalVatledger = $this->getDoctrine()->getRepository('TerminalbdNbrvatBundle:VatLedger')->getTotalColosing($post);

        $totalVatOldledger = $this->getDoctrine()->getRepository('TerminalbdNbrvatBundle:VatOldLedger')->getTotalOldColosing($post);

        $taxes = array(
            'inputTax'     => $inputTax,
            'totalInputTax'     => $totalInputTax,
            'outputRebateTax'       => $outputRebateTax,
            'totalOutputTax'    => $totalOutputTax,
            'totalCreditNote'   => $totalCreditNote,
            'totalDebitNote'    => $totalDebitNote,
            'totalVds'          => $totalVds,
            'totalAdjustment'          => $totalAdjustment,
            'totalAdjustmentBankingChannel'          => $totalAdjustmentBankingChannel,
            'increasingAdjustmentParticular'          => $increasingAdjustmentParticular,
            'decreasingAdjustmentParticular'          => $decreasingAdjustmentParticular,
            'totalVatledger'          => $totalVatledger,
            'totalVatOldledger'          => $totalVatOldledger,
            'netTaxPayable'          => $netTaxPayable,
            'bankTreasury'          => $bankTreasury,
        );
        $this->getDoctrine()->getRepository(TaxReturn::class)->updateTaxReturn($post,$taxes);

        $notes = $this->getDoctrine()->getRepository(Setting::class)->findBy(array('subForm'=> 1));

        return $this->render('@TerminalbdNbrvat/report/mushok91/mushok.html.twig', [
            'domain' => $domain,
            'entity'            => $post,
            'inputTax'          => $inputTax,
            'totalInputTax'     => $totalInputTax,
            'outputRebateTax'   => $outputRebateTax,
            'outputTax'         => $outputTax,
            'totalOutputTax'    => $totalOutputTax,
            'totalCreditNote'   => $totalCreditNote,
            'totalDebitNote'    => $totalDebitNote,
            'totalVds'          => $totalVds,
            'totalAdjustment'          => $totalAdjustment,
            'totalAdjustmentBankingChannel'          => $totalAdjustmentBankingChannel,
            'increasingAdjustmentParticular'          => $increasingAdjustmentParticular,
            'decreasingAdjustmentParticular'          => $decreasingAdjustmentParticular,
            'totalVatledger'          => $totalVatledger,
            'totalVatOldledger'          => $totalVatOldledger,
            'netTaxPayable'          => $netTaxPayable,
            'bankTreasury'          => $bankTreasury,
        ]);
    }


    /**
     * @Route("/mushuk/sub-form/9.1", methods={"GET"}, name="nbrvat_report_mushuk_9_1_sub_form")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT_SETTING')")
     */
    public function mushukSubForm91(Request $request): ? Response
    {
        $data = $_REQUEST;
        $post = '';
        $domain =$this->getUser()->getTerminal();
        $config = $this->getDoctrine()->getRepository(Nbrvat::class)->findConfig($this->getUser());
        $entities = $this->getDoctrine()->getRepository('TerminalbdNbrvatBundle:TaxReturn')->findBy(array('config' => $config));
        $types = array('part-3','part-4','part-5','part-6','part-9');
        $settings = $this->getDoctrine()->getRepository(SettingType::class)->getGroups($types);
        if(isset($data['taxPeriod']) and isset($data['subFormId'])) {
            $subFormID = $data['subFormId'];
            $post = $this->getDoctrine()->getRepository(Setting::class)->find($subFormID);
        }
        return $this->render('@TerminalbdNbrvat/report/mushok91/subFormIndex.html.twig', [
            'domain' => $domain,
            'entities' => $entities,
            'settings' => $settings,
            'note' => $post,
            'searchForm' => $data
        ]);
    }

    /**
     * @Route("/mushuk/sub-form/9.1/preview", methods={"GET"}, name="nbrvat_report_mushuk_9_1_sub_form_preview")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT_SETTING')")
     */
    public function mushukSubForm91Preview(Request $request): ? Response
    {
        $data = $_REQUEST;
        $domain =$this->getUser()->getTerminal();
        $inventory = $this->getDoctrine()->getRepository(Inventory::class)->findConfig($this->getUser());
        $nbrvat = $this->getDoctrine()->getRepository(Nbrvat::class)->findConfig($this->getUser());

        $types = array('part-3','part-4','part-5','part-6','part-9');
        if(isset($data['taxReturnId']) and isset($data['subFormId']) and !empty($data['subFormId'])) {

            $subFormID = $data['subFormId'];
            $taxReturnID = $data['taxReturnId'];
            $post = $this->getDoctrine()->getRepository(Setting::class)->findOneBy(array('id'=> $subFormID));
            $taxReturn = $this->getDoctrine()->getRepository(TaxReturn::class)->findOneBy(array('id'=> $taxReturnID));
            if ($post->getSettingType()->getSlug() == 'part-3') {
                $subForm = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:StockItem')->getOutputTaxSubForm($inventory,$taxReturn,$post);
            }elseif($post->getSettingType()->getSlug() == 'part-4'){
                $subForm = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:StockItem')->getInputTaxSubForm($inventory,$taxReturn,$post);
            }elseif($post->getSettingType()->getSlug() == 'part-5' and $post->getNoteNo() == "24" ){
                $subForm = $this->getDoctrine()->getRepository('TerminalbdNbrvatBundle:VdsTransaction')->getVdsSubForm($nbrvat,$taxReturn,"purchase");
            }elseif($post->getSettingType()->getSlug() == 'part-6' and $post->getNoteNo() == "29" ){
                $subForm = $this->getDoctrine()->getRepository('TerminalbdNbrvatBundle:VdsTransaction')->getVdsSubForm($nbrvat,$taxReturn,"sales");
            }elseif($post->getSettingType()->getSlug() == 'part-6' and $post->getNoteNo() == 30){
                $subForm = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:Purchase')->getSubFormVatAt($inventory,$taxReturn);
            }elseif($post->getSettingType()->getSlug() == 'part-9'){
                $subForm = $this->getDoctrine()->getRepository('TerminalbdNbrvatBundle:BankTreasury')->getBankTreasury($nbrvat,$taxReturn,$post->getSlug());
            }

        }else{

            $post    = "";
            $subForm    = "";
        }
        return $this->render('@TerminalbdNbrvat/report/mushok91/sub-form.html.twig', [
            'domain' => $domain,
            'note' => $post,
            'subForm' => $subForm,
            'searchForm' => $data
        ]);
    }

    /**
     * @Route("/mushuk/sub-form-adjustment/9.1", methods={"GET"}, name="nbrvat_report_mushuk_9_1_sub_form_adjustment")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT_SETTING')")
     */
    public function mushukSubFormAdjustment(Request $request): ? Response
    {
        $data = $_REQUEST;
        $post = '';
        $domain =$this->getUser()->getTerminal();
        $config = $this->getDoctrine()->getRepository(Nbrvat::class)->findConfig($this->getUser());
        $entities = $this->getDoctrine()->getRepository('TerminalbdNbrvatBundle:TaxReturn')->findBy(array('config' => $config));
        $types = array('decreasing-adjutment','increasing-adjutment');
        $settings = $this->getDoctrine()->getRepository(SettingType::class)->getGroups($types);
        if(isset($data['taxPeriod']) and isset($data['subFormId'])) {
            $subFormID = $data['subFormId'];
            $post = $this->getDoctrine()->getRepository(Setting::class)->find($subFormID);
        }
        return $this->render('@TerminalbdNbrvat/report/mushok91/subFormAdjustmentIndex.html.twig', [
            'domain' => $domain,
            'entities' => $entities,
            'settings' => $settings,
            'note' => $post,
            'searchForm' => $data
        ]);
    }

    /**
     * @Route("/mushuk/sub-form/9.1/adjustment-preview", methods={"GET"}, name="nbrvat_report_mushuk_9_1_sub_form_adjustment_preview")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT_SETTING')")
     */
    public function mushukSubForm91AdjustmentPreview(Request $request): ? Response
    {
        $data = $_REQUEST;
        $domain =$this->getUser()->getTerminal();
        $inventory = $this->getDoctrine()->getRepository(Inventory::class)->findConfig($this->getUser());
        $nbrvat = $this->getDoctrine()->getRepository(Nbrvat::class)->findConfig($this->getUser());
        if(isset($data['taxReturnId']) and isset($data['subFormId']) and !empty($data['subFormId'])) {
            $subFormID = $data['subFormId'];
            $taxReturnID = $data['taxReturnId'];
            $post = $this->getDoctrine()->getRepository(Setting::class)->findOneBy(array('id'=> $subFormID));
            $taxReturn = $this->getDoctrine()->getRepository(TaxReturn::class)->findOneBy(array('id'=> $taxReturnID));
            if ($post->getSettingType()->getSlug() == 'increasing-adjutment') {
                $subForm = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:SalesItemTax')->getSubFormTaxRefund($inventory,$taxReturn,'increasing');
            }elseif($post->getSettingType()->getSlug() == 'decreasing-adjutment'){
                $subForm = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:SalesItemTax')->getSubFormTaxRefund($inventory,$taxReturn,'decreasing');
            }

        }else{

            $post    = "";
            $subForm    = "";
        }
        return $this->render('@TerminalbdNbrvat/report/mushok91/sub-form-adjustment.html.twig', [
            'domain' => $domain,
            'note' => $post,
            'subForm' => $subForm,
            'searchForm' => $data
        ]);
    }



    /**
     * @Route("/mushuk/tr-6", methods={"GET"}, name="nbrvat_report_mushuk_tr")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT_SETTING')")
     */
    public function mushuktr6(Request $request): ? Response
    {
        $data = $_REQUEST;
        $domain =$this->getUser()->getTerminal();
        $config = $this->getDoctrine()->getRepository(Nbrvat::class)->findConfig($this->getUser());
        $entities = $this->getDoctrine()->getRepository(BankTreasury::class)->findBy(array('config'=>$config));
        if(isset($data['invoice']) and !empty($data['invoice'])){
            $post = $this->getDoctrine()->getRepository(BankTreasury::class)->findOneBy(array('config'=>$config,'id'=>$data['invoice']));
        }else{
            $post = "";
        }
        return $this->render('@TerminalbdNbrvat/report/tr6/index.html.twig', [
            'domain' => $domain,
            'post' => $post,
            'entities' => $entities,
            'searchForm' => $data
        ]);
    }


    /**
     * @Route("/{id}/{mode}/mushuk/tr-6/preview", methods={"GET"}, name="nbrvat_report_mushuk_tr_preview")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT_REPORT')")
     */

    public function mushuktr6Print(Request $request, BankTreasury $treasury , $mode): Response
    {
        /* @var $domain Terminal */
        $confManager = new ConfigureManager();
        $domain =$this->getUser()->getTerminal();
        $config = $this->getDoctrine()->getRepository(Nbrvat::class)->findConfig($this->getUser());
        $inWords = $confManager->intToWords($treasury->getAmount());
        return $this->render('@TerminalbdNbrvat/report/tr6/mushok.html.twig', [
            'domain' => $domain,
            'entity' => $treasury,
            'inWords' => $inWords,
        ]);

    }


    /**
     * @Route("/mushuk/debite-note", methods={"GET"}, name="nbrvat_report_purchase_return")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT_REPORT')")
     */

    public function purchaseReturnReport()
    {
        /* @var $domain Terminal */
        $confManager = new ConfigureManager();
        $domain =$this->getUser()->getTerminal();
        $data = $_REQUEST;
        $config = $this->getDoctrine()->getRepository(Inventory::class)->findConfig($this->getUser());
        if(isset($data['startDate']) and !empty($data['startDate']) and isset($data['endDate']) and !empty($data['endDate'])){
            $entities = $this->getDoctrine()->getRepository(PurchaseReturnItem::class)->mushukPurchaseReturnItem($config,$data);
        }else{
            $entities = "";
        }
        return $this->render('@TerminalbdNbrvat/report/purchase/purchase-return.html.twig', [
            'domain' => $domain,
            'entities' => $entities,
            'searchForm' => $data
        ]);
    }

    /**
     * @Route("/mushuk/debite-note-preview", methods={"GET"}, name="nbrvat_report_purchase_return_preview")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT_REPORT')")
     */

    public function purchaseReturnReportPrint()
    {
        /* @var $domain Terminal */

        $confManager = new ConfigureManager();
        $domain =$this->getUser()->getTerminal();
        $data = $_REQUEST;
        $config = $this->getDoctrine()->getRepository(Inventory::class)->findConfig($this->getUser());
        if(isset($data['startDate']) and !empty($data['startDate']) and isset($data['endDate']) and !empty($data['endDate'])){
            $purchase = $this->getDoctrine()->getRepository(PurchaseReturnItem::class)->mushukPurchaseReturnItem($config,$data);
        }else{
            $purchase = "";
        }
        return $this->render('@TerminalbdNbrvat/report/purchase/purchase-return-table.html.twig', [
            'domain' => $domain,
            'entities' => $purchase,
            'searchForm' => $data
        ]);

    }

    /**
     * @Route("/mushuk/credit-note", methods={"GET"}, name="nbrvat_report_sales_return")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT_REPORT')")
     */

    public function salesReturnReport()
    {
        /* @var $domain Terminal */
        $confManager = new ConfigureManager();
        $domain =$this->getUser()->getTerminal();
        $data = $_REQUEST;
        $config = $this->getDoctrine()->getRepository(Inventory::class)->findConfig($this->getUser());
        if(isset($data['startDate']) and !empty($data['startDate']) and isset($data['endDate']) and !empty($data['endDate'])){
            $entities = $this->getDoctrine()->getRepository(PurchaseReturnItem::class)->mushukPurchaseReturnItem($config,$data);
        }else{
            $entities = "";
        }
        return $this->render('@TerminalbdNbrvat/report/purchase/purchase-return.html.twig', [
            'domain' => $domain,
            'entities' => $entities,
            'searchForm' => $data
        ]);
    }

    /**
     * @Route("/mushuk/credit-note-preview", methods={"GET"}, name="nbrvat_report_sales_return_preview")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT_REPORT')")
     */

    public function salesReturnReportPrint()
    {
        /* @var $domain Terminal */
        $confManager = new ConfigureManager();
        $domain =$this->getUser()->getTerminal();
        $data = $_REQUEST;
        $config = $this->getDoctrine()->getRepository(Inventory::class)->findConfig($this->getUser());
        if(isset($data['startDate']) and !empty($data['startDate']) and isset($data['endDate']) and !empty($data['endDate'])){
            $purchase = $this->getDoctrine()->getRepository(PurchaseReturnItem::class)->mushukPurchaseReturnItem($config,$data);
        }else{
            $purchase = "";
        }
        return $this->render('@TerminalbdNbrvat/report/purchase/purchase-return-table.html.twig', [
            'domain' => $domain,
            'entity' => $purchase,
            'searchForm' => $data
        ]);

    }

}
