<?php

namespace Terminalbd\NbrvatBundle\Controller;
use App\Entity\Application\Nbrvat;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\NbrvatBundle\Entity\Setting;
use Terminalbd\NbrvatBundle\Entity\TaxSetup;
use Terminalbd\NbrvatBundle\Form\SettingFormType;


/**
 * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT_SETTING')")
 * @Route("/nbrvat/setting")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class SettingController extends AbstractController
{
    /**
     * @Route("/", methods={"GET"}, name="nbrvat_setting")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT_SETTING')")
     */
    public function index(Request $request): Response
    {
        $entities = $this->getDoctrine()->getRepository(Setting::class)->findAll();
        return $this->render('@TerminalbdNbrvat/setting/index.html.twig', [
            'entities' => $entities,
        ]);
    }

    /**
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT_SETTING')")
     * @Route("/new", methods={"GET", "POST"}, name="nbrvat_setting_new")
     */
    public function new(Request $request): Response
    {

        $entity = new Setting();
        $data = $request->request->all();
        $form = $this->createForm(SettingFormType::class , $entity)
            ->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $this->addFlash('success', 'post.created_successfully');
            if ($form->get('SaveAndCreate')->isClicked()) {
                return $this->redirectToRoute('nbrvat_setting_new');
            }
            return $this->redirectToRoute('nbrvat_setting');
        }
        return $this->render('@TerminalbdNbrvat/setting/new.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/edit", methods={"GET", "POST"}, name="nbrvat_setting_edit")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT_SETTING')")
     */
    public function edit(Request $request, Setting $entity): Response
    {
        $data = $request->request->all();
        $form = $this->createForm(SettingFormType::class, $entity)
            ->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'post.updated_successfully');
            if ($form->get('SaveAndCreate')->isClicked()) {
                return $this->redirectToRoute('nbrvat_setting_edit', ['id' => $entity->getId()]);
            }

            return $this->redirectToRoute('nbrvat_setting');
        }
        return $this->render('@TerminalbdNbrvat/setting/new.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/delete", methods={"GET"}, name="nbrvat_setting_delete")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT_SETTING')")
     */
    public function delete($id): Response
    {
        $entity = $this->getDoctrine()->getRepository(Setting::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($entity);
        $em->flush();
        $this->addFlash('success', 'post.deleted_successfully');
        return new Response('Success');
    }


    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/tax-setup", methods={"GET", "POST"}, name="nbrvat_setting_taxsetup")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_inventory_setting')")
     */
    public function taxSetup(Request $request): Response
    {
        $config = $this->getDoctrine()->getRepository(Nbrvat::class)->findConfig($this->getUser());

        $outputs = $this->getDoctrine()->getRepository(Setting::class)->getChildObjectRecords('part-3');
        $inputs = $this->getDoctrine()->getRepository(Setting::class)->getChildObjectRecords('part-4');
        $exist = $this->getDoctrine()->getRepository(TaxSetup::class)->findBy(array('config'=>$config));
        if(empty($exist)){
            $this->getDoctrine()->getRepository('TerminalbdNbrvatBundle:TaxSetup')->taxSetup($config,$outputs,$inputs);
            $exist = $this->getDoctrine()->getRepository(TaxSetup::class)->findBy(array('config' => $config));
        }
        $taxes = array();
        foreach ($exist as $row){
            $taxes[$row->getTax()->getNoteNo()] = $row;
        }
        return $this->render('@TerminalbdNbrvat/setting/taxSetup.html.twig', [
            'outputs' => $outputs,
            'inputs' => $inputs,
            'taxes' => $taxes,
            'actionUrl' => $this->generateUrl('nbrvat_setting_taxsetup_ajax'),

        ]);
    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/ajax-submit",methods={"GET", "POST"}, name="nbrvat_setting_taxsetup_ajax")
     * @Security("is_granted('ROLE_DOMAIN') or (is_granted('ROLE_NBRVAT_SETTING') and is_granted('ROLE_NBRVAT_CREATE'))")

     */

    public function ajaxSubmit(Request $request): Response
    {
        $data = $_REQUEST;
        $data = $request->request->all();
        $item = $data['item'];
        $entity = $this->getDoctrine()->getRepository(TaxSetup::class)->find($item);
        $entity->setInputMode($data['inputMode']);
        $entity->setInputValue($data['inputValue']);
        $entity->setTaxMode($data['taxMode']);
        $em = $this->getDoctrine()->getManager();
        $em->flush();
        return new Response('success');

    }


}
