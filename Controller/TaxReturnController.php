<?php
namespace Terminalbd\NbrvatBundle\Controller;

use App\Entity\Admin\Terminal;
use App\Entity\Application\Inventory;
use App\Entity\Application\Nbrvat;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Terminalbd\AccountingBundle\Repository\AccountBankRepository;
use Terminalbd\NbrvatBundle\Entity\BankTreasury;
use Terminalbd\NbrvatBundle\Entity\Setting;
use Terminalbd\NbrvatBundle\Entity\TaxReturn;
use Terminalbd\NbrvatBundle\Form\TaxReturnFormType;
use Terminalbd\NbrvatBundle\Form\TreasuaryFormType;

/**
 * @Route("/nbrvat/tax-return")
 * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT')")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class TaxReturnController extends AbstractController
{
    /**
     * @Route("/", methods={"GET"}, name="nbrvat_taxreturn")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT_SETTING')")
     */
    public function index(Request $request): ? Response
    {
        $config = $this->getDoctrine()->getRepository(Nbrvat::class)->findConfig($this->getUser());
        $posts = $this->getDoctrine()->getRepository('TerminalbdNbrvatBundle:TaxReturn')->findBy(array('config' => $config));
        return $this->render('@TerminalbdNbrvat/taxreturn/index.html.twig', [
            'entities' => $posts
        ]);
    }

    /**
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT_SETTING')")
     * @Route("/new", methods={"GET", "POST"}, name="nbrvat_taxreturn_new")
     */

    public function create(Request $request): Response
    {
        $entity = new TaxReturn();
        /* @var $terminal Terminal */
        $terminal = $this->getUser()->getTerminal();
        $config = $this->getDoctrine()->getRepository(Nbrvat::class)->findConfig($this->getUser());
        $em = $this->getDoctrine()->getManager();
        $entity->setTerminal($terminal);
        $entity->setConfig($config);
        $receiveDate = new \DateTime('now');
        $entity->setSubmissionDate($receiveDate);
        $entity->setProcess('created');
        $entity->setSignaturePerson($terminal->getSignaturePerson());
        $entity->setSignatureDesignation($terminal->getSignatureDesignation());
        $entity->setSignatureMobile($terminal->getSignatureMobile());
        $entity->setSignatureNid($terminal->getSignatureNid());
        $entity->setSignatureEmail($terminal->getSignatureEmail());
        $entity->setCreatedBy($this->getUser());
        $em->persist($entity);
        $em->flush();
        return $this->redirectToRoute('nbrvat_taxreturn_edit',['id'=> $entity->getId()]);

    }

    /**
     * @Security("is_granted('ROLE_DOMAIN') or (is_granted('ROLE_NBRVAT_SETTING') and is_granted('ROLE_NBRVAT_CREATE'))")
     * @Route("/{id}/process", methods={"GET", "POST"}, name="nbrvat_taxreturn_edit")
     */
    public function edit(TaxReturn $post , Request $request): Response
    {

        $inventory = $this->getDoctrine()->getRepository(Inventory::class)->findConfig($this->getUser());
        $config = $this->getDoctrine()->getRepository(Nbrvat::class)->findConfig($this->getUser());
        $form = $this->createForm(TaxReturnFormType::class , $post)
            ->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $post->setConfig($config);
            $em->persist($post);
            $em->flush();
            $this->addFlash('success', 'post.created_successfully');
            if ($form->get('SaveAndCreate')->isClicked()) {
                return $this->redirectToRoute('nbrvat_taxreturn_new');
            }
            return $this->redirectToRoute('nbrvat_taxreturn_new');
        }

        if($post->getTaxPeriod()){

            $inputTax = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:StockItem')->getInputTax($inventory,$post);
            $totalInputTax = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:StockItem')->getTotalInputTax($inventory,$post);

            $totalDebitNote = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:PurchaseReturn')->getTotalDeditNote($inventory,$post);

            $outputTax = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:StockItem')->getOutputTax($inventory,$post);

            $outputRebateTax = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:StockItem')->getOutputRebateTax($inventory,$post);

            $this->getDoctrine()->getRepository('TerminalbdNbrvatBundle:VatAdjustment')->getVatRefundExport($post,$outputRebateTax);

            $this->getDoctrine()->getRepository('TerminalbdNbrvatBundle:VatAdjustment')->getVatRebateForSales($post,$outputRebateTax);

            $totalOutputTax = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:StockItem')->getTotalOutputTax($inventory,$post);

            $totalCreditNote = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:SalesReturn')->getTotalCreditNote($inventory,$post);

            $totalVds = $this->getDoctrine()->getRepository('TerminalbdNbrvatBundle:VdsTransaction')->getTotalVds($post);

            $totalAdjustment = $this->getDoctrine()->getRepository('TerminalbdNbrvatBundle:VatAdjustment')->getTotalAdjustment($post);

            $decreasingAdjustmentParticular = $this->getDoctrine()->getRepository('TerminalbdNbrvatBundle:VatAdjustment')->getAdjustmentParticular($post,"decreasing-adjutment");

            $increasingAdjustmentParticular = $this->getDoctrine()->getRepository('TerminalbdNbrvatBundle:VatAdjustment')->getAdjustmentParticular($post,"increasing-adjutment");

            $totalAdjustmentBankingChannel = $this->getDoctrine()->getRepository('TerminalbdNbrvatBundle:VatAdjustment')->getTotalAdjustmentBankingChannel($post);

            $netTaxPayable = $this->getDoctrine()->getRepository('TerminalbdNbrvatBundle:NetTaxPayable')->getNetTaxPayableSummary($post);

            $bankTreasury = $this->getDoctrine()->getRepository('TerminalbdNbrvatBundle:BankTreasury')->getBankTreasurySummary($post);

            $totalVatledger = $this->getDoctrine()->getRepository('TerminalbdNbrvatBundle:VatLedger')->getTotalColosing($post);

            $totalVatOldledger = $this->getDoctrine()->getRepository('TerminalbdNbrvatBundle:VatOldLedger')->getTotalOldColosing($post);

            $taxes = array(
                'inputTax'     => $inputTax,
                'totalInputTax'     => $totalInputTax,
                'outputRebateTax'       => $outputRebateTax,
                'totalOutputTax'    => $totalOutputTax,
                'totalCreditNote'   => $totalCreditNote,
                'totalDebitNote'    => $totalDebitNote,
                'totalVds'          => $totalVds,
                'totalAdjustment'          => $totalAdjustment,
                'totalAdjustmentBankingChannel'          => $totalAdjustmentBankingChannel,
                'increasingAdjustmentParticular'          => $increasingAdjustmentParticular,
                'decreasingAdjustmentParticular'          => $decreasingAdjustmentParticular,
                'totalVatledger'          => $totalVatledger,
                'totalVatOldledger'          => $totalVatOldledger,
                'netTaxPayable'          => $netTaxPayable,
                'bankTreasury'          => $bankTreasury,
            );
            $this->getDoctrine()->getRepository(TaxReturn::class)->updateTaxReturn($post,$taxes);


            return $this->render('@TerminalbdNbrvat/taxreturn/new.html.twig', [
                'entity'            => $post,
                'inputTax'          => $inputTax,
                'totalInputTax'     => $totalInputTax,
                'outputRebateTax'       => $outputRebateTax,
                'outputTax'         => $outputTax,
                'totalOutputTax'    => $totalOutputTax,
                'totalCreditNote'   => $totalCreditNote,
                'totalDebitNote'    => $totalDebitNote,
                'totalVds'          => $totalVds,
                'totalAdjustment'          => $totalAdjustment,
                'totalAdjustmentBankingChannel'          => $totalAdjustmentBankingChannel,
                'increasingAdjustmentParticular'          => $increasingAdjustmentParticular,
                'decreasingAdjustmentParticular'          => $decreasingAdjustmentParticular,
                'totalVatledger'          => $totalVatledger,
                'totalVatOldledger'          => $totalVatOldledger,
                'netTaxPayable'          => $netTaxPayable,
                'bankTreasury'          => $bankTreasury,
                'form'              => $form->createView(),
                'actionUrl' => $this->generateUrl('nbrvat_taxreturn_edit',array('id'=> $post->getId())),
                'dataAction' => $this->generateUrl('nbrvat_taxreturn_ajax',array('id'=> $post->getId())),

            ]);
        }else{
            return $this->render('@TerminalbdNbrvat/taxreturn/new.html.twig', [
                'entity'            => $post,
                'form'              => $form->createView(),
                'actionUrl' => $this->generateUrl('nbrvat_taxreturn_edit',array('id'=> $post->getId())),
                'dataAction' => $this->generateUrl('nbrvat_taxreturn_ajax',array('id'=> $post->getId())),

            ]);
        }

    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/ajax-submit",methods={"GET", "POST"}, name="nbrvat_taxreturn_ajax")
     * @Security("is_granted('ROLE_DOMAIN') or (is_granted('ROLE_NBRVAT_SETTING') and is_granted('ROLE_NBRVAT_CREATE'))")

     */

    public function ajaxSubmit(Request $request, TaxReturn $post): Response
    {
        $data = $_REQUEST;
        $data = $request->request->all();
        $inventory = $this->getDoctrine()->getRepository(Inventory::class)->findConfig($this->getUser());
        $config = $this->getDoctrine()->getRepository(Nbrvat::class)->findConfig($this->getUser());
        $form = $this->createForm(TaxReturnFormType::class , $post);
        $form->handleRequest($request);
        $taxPeriod = $form->get('taxPeriod')->getData();
        $exist = $this->getDoctrine()->getRepository(TaxReturn::class)->findOneBy(array('config'=> $config,'taxPeriod' => $taxPeriod));

        $dataEx = explode('-',$taxPeriod);
        $date = strtotime("{$dataEx[1]}-{$dataEx[0]}-01");
        if(empty($exist) and time() > strtotime($date) ){
            $em = $this->getDoctrine()->getManager();
            $d = date('Y-m-d',$date);
            $compareTo = new \DateTime($d);
            $post->setTaxMonth($compareTo);
            $em->persist($post);
            $em->flush();
            return new Response('success');
        }else{
            $em = $this->getDoctrine()->getManager();
            $isRefund = isset($data['isRefund']) ? $data['isRefund']:0;
            $post->setIsRefund($isRefund);
            $em->flush();
            return new Response('invalid');
        }

    }



    /**
     * Deletes a AccountBank entity.
     *
     * @Route("/{id}/delete",methods={"GET", "POST"}, name="nbrvat_taxreturn_delete")
     * @Security("is_granted('ROLE_DOMAIN') or (is_granted('ROLE_NBRVAT_SETTING') and is_granted('ROLE_NBRVAT_DELETE'))")
     */
    public function delete($id): Response
    {
        $post = $this->getDoctrine()->getRepository(TaxReturn::class)->findOneBy(['id'=> $id]);
        $em = $this->getDoctrine()->getManager();
        $em->remove($post);
        $em->flush();
        $this->addFlash('success', 'post.deleted_successfully');
        return new Response('Success');
    }

    /**
     * Process a Porduction Issue entity.
     *
     * @Route("/{id}/process", methods={"GET"}, name="nbrvat_taxreturn_process")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT')")
     */
    public function process($id): Response
    {
        $entity = $this->getDoctrine()->getRepository(BankTreasury::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        $entity->setApprovedBy($this->getUser());
        $entity->setProcess('approved');
        $em->flush();
        return new Response('Success');

    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{mode}/process-group", methods={"GET"}, name="nbrvat_taxreturn_process_group")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_ISSUE')")
     */
    public function processGroup($mode): Response
    {
        $em = $this->getDoctrine()->getManager();
        $ids = explode(',',$_REQUEST['ids']);
        foreach ($ids as $key => $id):
            $post = $this->getDoctrine()->getRepository("TerminalbdNbrvatBundle:BankTreasury")->find($id);
            $entity->setApprovedBy($this->getUser());
            $entity->setProcess('approved');
            $em->flush();
        endforeach;
        return new Response('Success');

    }


    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/{tax}/output-sub-form",methods={"GET"}, name="nbrvat_taxreturn_subform")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT')")
     */
    public function subFormOutput(Request $request,$id, $tax): Response
    {
        $em = $this->getDoctrine()->getManager();

        $inventory = $this->getDoctrine()->getRepository(Inventory::class)->findConfig($this->getUser());
        $taxReturn = $this->getDoctrine()->getRepository(TaxReturn::class)->find($tax);
        $post = $this->getDoctrine()->getRepository(Setting::class)->findOneBy(array('noteNo'=> $id));
        $subForm = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:StockItem')->getOutputTaxSubForm($inventory,$taxReturn,$post);

        return $this->render('@TerminalbdNbrvat/taxreturn/sub-form.html.twig',['post' => $post,'subForm'=> $subForm]);
    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/{tax}/input-sub-form",methods={"GET"}, name="nbrvat_taxreturn_subform_input")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT')")
     */
    public function subFormInput(Request $request,$id, $tax): Response
    {
        $em = $this->getDoctrine()->getManager();

        $inventory = $this->getDoctrine()->getRepository(Inventory::class)->findConfig($this->getUser());
        $taxReturn = $this->getDoctrine()->getRepository(TaxReturn::class)->find($tax);
        $post = $this->getDoctrine()->getRepository(Setting::class)->findOneBy(array('noteNo'=> $id));
        $subForm = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:StockItem')->getInputTaxSubForm($inventory,$taxReturn,$post);

        return $this->render('@TerminalbdNbrvat/taxreturn/sub-form.html.twig',['post' => $post,'subForm'=> $subForm]);
    }


    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/{mode}/input-sub-form-vds",methods={"GET"}, name="nbrvat_taxreturn_subform_vds")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT')")
     */
    public function subFormVds(Request $request,$id,$mode): Response
    {
        $em = $this->getDoctrine()->getManager();

        $config = $this->getDoctrine()->getRepository(Nbrvat::class)->findConfig($this->getUser());
        $taxReturn = $this->getDoctrine()->getRepository(TaxReturn::class)->find($id);
        $subForm = $this->getDoctrine()->getRepository('TerminalbdNbrvatBundle:VdsTransaction')->getVdsSubForm($config,$taxReturn,$mode);
        return $this->render('@TerminalbdNbrvat/taxreturn/vds-sub-form.html.twig',['post' => $taxReturn,'subForm'=> $subForm,'mode' => $mode]);


    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/{mode}/input-sub-form-debit-credit",methods={"GET"}, name="nbrvat_taxreturn_subform_debit_credit")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT')")
     */
    public function subFormDebitCreditNote(Request $request,$id,$mode): Response
    {
        $em = $this->getDoctrine()->getManager();

        $inventory = $this->getDoctrine()->getRepository(Inventory::class)->findConfig($this->getUser());
        $taxReturn = $this->getDoctrine()->getRepository(TaxReturn::class)->find($id);
        $subForm = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:StockItem')->getInputTaxSubForm($inventory,$taxReturn,$post);

        return $this->render('@TerminalbdNbrvat/taxreturn/sub-form.html.twig',['post' => $post,'subForm'=> $subForm]);
    }


     /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/input-sub-form-at-vat",methods={"GET"}, name="nbrvat_taxreturn_subform_at_vat")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT')")
     */

    public function subFormAtVateNote(Request $request,$id): Response
    {
        $em = $this->getDoctrine()->getManager();
        $inventory = $this->getDoctrine()->getRepository(Inventory::class)->findConfig($this->getUser());
        $taxReturn = $this->getDoctrine()->getRepository(TaxReturn::class)->find($id);
        $subForm = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:Purchase')->getSubFormVatAt($inventory,$taxReturn);
        return $this->render('@TerminalbdNbrvat/taxreturn/at-sub-form.html.twig',['subForm' => $subForm]);
    }


    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/{tax}/input-sub-form-deposite-treasury",methods={"GET"}, name="nbrvat_taxreturn_subform_deposite_treasury")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT')")
     */
    public function subFormDepositeTreasury(Request $request, $id , $tax): Response
    {
        $em = $this->getDoctrine()->getManager();
        $config = $this->getDoctrine()->getRepository(Nbrvat::class)->findConfig($this->getUser());
        $post = $this->getDoctrine()->getRepository(Setting::class)->findOneBy(array('slug'=> $id));
        $taxReturn = $this->getDoctrine()->getRepository(TaxReturn::class)->find($tax);
        $subForm = $this->getDoctrine()->getRepository('TerminalbdNbrvatBundle:BankTreasury')->getBankTreasury($config,$taxReturn,$id);
        return $this->render('@TerminalbdNbrvat/taxreturn/treasury-sub-form.html.twig',
            ['post' => $post,'subForm'=> $subForm]
        );
    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/{mode}/input-sub-form-refund-rebate",methods={"GET"}, name="nbrvat_taxreturn_subform_refund_rebate")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT')")
     */
    public function subFormRefundRebateTax(Request $request, TaxReturn $post, $mode)
    {
        $em = $this->getDoctrine()->getManager();
        $config = $this->getDoctrine()->getRepository(Nbrvat::class)->findConfig($this->getUser());
        $post = $this->getDoctrine()->getRepository(Setting::class)->findOneBy(array('slug'=> $id));
        $taxReturn = $this->getDoctrine()->getRepository(TaxReturn::class)->find($tax);
        $subForm = $this->getDoctrine()->getRepository('TerminalbdNbrvatBundle:BankTreasury')->getBankTreasury($config,$taxReturn,$id);
        return $this->render('@TerminalbdNbrvat/taxreturn/treasury-sub-form.html.twig',
            ['post' => $post,'subForm'=> $subForm]
        );
    }



}
