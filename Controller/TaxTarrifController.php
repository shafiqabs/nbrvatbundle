<?php

namespace Terminalbd\NbrvatBundle\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\InventoryBundle\Repository\TaxTariffRepository;
use Terminalbd\NbrvatBundle\Entity\TaxTariff;

/**
 * @Route("/nbrvat/taxtarrif")
 * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT')")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class TaxTarrifController extends AbstractController
{


    /**
     * @Route("/", methods={"GET"}, name="nbrvat_taxtarrif")
     */
    public function index(Request $request): Response
    {
        return $this->render('@TerminalbdNbrvat/taxtarrif/index.html.twig');
    }


    /**
     * @Route("/data-table", methods={"GET", "POST"}, name="nbrvat_taxtarrif_data_table")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT')")
     */

    public function dataTable(Request $request)
    {

        $query = $_REQUEST;
        $iTotalRecords = $this->getDoctrine()->getRepository("TerminalbdNbrvatBundle:TaxTariff")->rowCount($query);

        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);
        $records = array();
        $records["data"] = array();
        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $columnIndex = $_POST['order'][0]['column']; // Column index
        $columnName = $_POST['columns'][$columnIndex]['name']; // Column name
        $columnSortOrder = $_POST['order'][0]['dir']; // asc or desc

        $parameter = array("offset" => $iDisplayStart,'limit'=> $iDisplayLength,'orderBy' => $columnName,"order" => $columnSortOrder);

        $result = $this->getDoctrine()->getRepository('TerminalbdNbrvatBundle:TaxTariff')->findWidthSearch($parameter,$query);

        $i = $iDisplayStart > 0  ? ($iDisplayStart+1) : 1;

        /* @var $post TaxTariff */

        foreach ($result as $post):

            $records["data"][] = array(
                $id                 = $i,
                $hsCode        = $post['hscode'],
                $name          = $post['name'],
                $cd            = $post['cd'],
                $sd            = $post['sd'],
                $vat           = $post['vat'],
                $ait           = $post['ait'],
                $rd            = $post['rd'],
                $at           = $post['at'],
                $tti           = $post['tti'],
            );
            $i++;

        endforeach;

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
        return new JsonResponse($records);
    }



}
