<?php
namespace Terminalbd\NbrvatBundle\Controller;

use App\Entity\Admin\Terminal;
use App\Entity\Application\Accounting;
use App\Entity\Application\Inventory;
use App\Entity\Application\Nbrvat;
use App\Entity\Core\Setting;
use App\Entity\Core\Vendor;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\InventoryBundle\Entity\InvoiceKeyValue;
use Terminalbd\InventoryBundle\Entity\Item;
use Terminalbd\InventoryBundle\Entity\Purchase;
use Terminalbd\InventoryBundle\Entity\PurchaseItem;
use Terminalbd\InventoryBundle\Entity\Sales;
use Terminalbd\InventoryBundle\Form\PurchaseImportFormType;
use Terminalbd\InventoryBundle\Form\PurchaseLocalFormType;
use Terminalbd\InventoryBundle\Repository\ItemRepository;
use Terminalbd\NbrvatBundle\Entity\VdsCertificate;
use Terminalbd\NbrvatBundle\Entity\VdsTransaction;
use Terminalbd\NbrvatBundle\Form\VdsPurchaseFormType;
use Terminalbd\NbrvatBundle\Form\VdsSalesFormType;

/**
 * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT')")
 * @Route("/nbrvat/vds")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class VdsController extends AbstractController
{


    /**
     * @Route("/", methods={"GET"}, name="nbrvat_vds")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT')")
     */
    public function index(Request $request): Response
    {
        $terminal = $this->getUser()->getTerminal();
        return $this->render('@TerminalbdNbrvat/vds/index.html.twig');
    }

    /**
     * @Route("/certificate", methods={"GET"}, name="nbrvat_vds_certificate")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT')")
     */
    public function certificate(Request $request): Response
    {
        $terminal = $this->getUser()->getTerminal();
        return $this->render('@TerminalbdNbrvat/vds/certificate.html.twig');
    }

    /**
     * @Security("is_granted('ROLE_DOMAIN') or (is_granted('ROLE_NBRVAT_SETTING') and is_granted('ROLE_NBRVAT_CREATE'))")
     * @Route("/{mode}/new", methods={"GET", "POST"}, name="nbrvat_vds_new")
     */
    public function new(Request $request,$mode): Response
    {
        $config = $this->getDoctrine()->getRepository(Nbrvat::class)->findConfig($this->getUser());
        $post = new VdsTransaction();
        if($mode == 'purchase'){
            $form = $this->createForm(VdsPurchaseFormType::class , $post, array('nbrvat' => $config))->add('SaveAndCreate', SubmitType::class);
        }else{
            $form = $this->createForm(VdsSalesFormType::class , $post , array('nbrvat' => $config))->add('SaveAndCreate', SubmitType::class);
        }
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $post->setConfig($config);
            $em->persist($post);
            $em->flush();
            $this->addFlash('success', 'post.created_successfully');
            if ($form->get('SaveAndCreate')->isClicked()) {
                return $this->redirectToRoute('nbrvat_vds_certificate');
            }
            return $this->redirectToRoute('nbrvat_vds_new',array('mode' => $mode));
        }
        return $this->render('@TerminalbdNbrvat/vds/new.html.twig', [
            'post' => $post,
            'form' => $form->createView(),
        ]);
    }




    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/cretificate/show", methods={"GET"}, name="nbrvat_vds_certificate_show")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_PURCHASE')")
     */

    public function vdsCertificateShow(Request $request, VdsTransaction $certificate)
    {

        $domain =$this->getUser()->getTerminal();
        return $this->render('@TerminalbdNbrvat/report/mushok66/mushok.html.twig',['entity' => $certificate, 'domain' => $domain]);
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/cretificate/search", methods={"GET"}, name="nbrvat_vds_certificate_search")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_PURCHASE')")
     */

    public function vdsCertificateSearch(Request $request)
    {
        $id = $_REQUEST['id'];
        $certificate = $this->getDoctrine()->getRepository(VdsCertificate::class)->find($id);
        /* @var $purchase Purchase */
        $purchase = $certificate->getPurchase();
        if($purchase){
            if($certificate->getBalance() > 0 ){
                $data = array(
                    'balance' => $certificate->getBalance(),
                    'vendorName' => $purchase->getVendor()->getCompanyName(),
                    'vendorAddress' => $purchase->getVendor()->getAddress(),
                    'vendorBin' => $purchase->getVendor()->getBinNo(),
                    'challanNo' => $purchase->getChallanNo(),
                    'challanDate' => $purchase->getReceiveDate()->format('d-m-Y'),
                );
            }else{
                $data = array(
                    'balance' => 'invalid',
                    'vendorName' => $purchase->getVendor()->getCompanyName(),
                    'vendorAddress' => $purchase->getVendor()->getAddress(),
                    'vendorBin' => $purchase->getVendor()->getBinNo(),
                    'challanNo' => $purchase->getChallanNo(),
                    'challanDate' => $purchase->getReceiveDate()->format('d-m-Y'),
                );
            }
        }else{

            /* @var $sales Sales */
            $sales = $certificate->getSales();
            if($certificate->getBalance() > 0 ){
                $data = array(
                    'balance' => $certificate->getBalance(),
                    'vendorName' => $sales->getCustomer()->getCompanyName(),
                    'vendorAddress' => $sales->getCustomer()->getAddress(),
                    'vendorBin' => $sales->getCustomer()->getBinNo(),
                    'challanNo' => $sales->getInvoice(),
                    'challanDate' => $certificate->getUpdated()->format('d-m-Y'),
                );
            }else{
                $data = array(
                    'balance' => 'invalid',
                    'vendorName' => $sales->getCustomer()->getCompanyName(),
                    'vendorAddress' => $sales->getCustomer()->getAddress(),
                    'vendorBin' => $sales->getCustomer()->getBinNo(),
                    'challanNo' => $sales->getInvoice(),
                    'challanDate' => $certificate->getUpdated()->format('d-m-Y'),
                );
            }
        }


        return new Response(json_encode($data));

    }


    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/delete", methods={"GET"}, name="nbrvat_vds_delete")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_SALES')")
     */
    public function delete($id): Response
    {
        $post = $this->getDoctrine()->getRepository(VdsTransaction::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($post);
        $em->flush();
        $this->addFlash('success', 'post.deleted_successfully');
        return new Response('Success');
    }


    /**
     * Approved a Setting entity.
     *
     * @Route("/{id}/process", methods={"GET"}, name="nbrvat_vds_process")
     * @Security("is_granted('ROLE_INVENTORY_SALES') or is_granted('ROLE_DOMAIN')")
     */
    public function process($id): Response
    {
        $entity = $this->getDoctrine()->getRepository(VdsTransaction::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        $balance = $entity->getVdsCertificate()->getAmount() + $entity->getAmount();
        if ($entity->getProcess() == 'created' and $entity->getVdsCertificate()->isClose() == 1 and   $entity->getVdsCertificate()->getTotal() >= $balance ){
            $entity->setCheckedBy($this->getUser());
            $entity->setProcess('checked');
            $em->persist($entity);
            $em->flush();
        }elseif ($entity->getProcess() == 'checked' and $entity->getVdsCertificate()->isClose() == 1 and   $entity->getVdsCertificate()->getTotal() >= $balance){
            $entity->setApprovedBy($this->getUser());
            $entity->setProcess('approved');
            $em->persist($entity);
            $em->flush();
            $amount = $this->getDoctrine()->getRepository('TerminalbdNbrvatBundle:VdsCertificate')->totalAmount($entity);
            return new Response('success');
        }
        return new Response('invalid');
    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/generate-certificate",methods={"GET"}, name="nbrvat_vds_generate_certificate")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT')")
     */
    public function certificateList(Request $request,VdsCertificate $post): Response
    {
        $em = $this->getDoctrine()->getManager();

        return $this->render('@TerminalbdNbrvat/vds/certificate-show.html.twig',['post' => $post]);
    }

    /**
     * @Route("/data-vds-table", methods={"GET", "POST"}, name="nbrvat_vds_data_table")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT')")
     */

    public function vdsDataTable(Request $request)
    {

        $query = $_REQUEST;
        $config = $this->getDoctrine()->getRepository(Nbrvat::class)->findConfig($this->getUser());
        $iTotalRecords = $this->getDoctrine()->getRepository(VdsCertificate::class)->count(array('config'=> $config));

        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);
        $records = array();
        $records["data"] = array();
        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $columnIndex = $_POST['order'][0]['column']; // Column index
        $columnName = $_POST['columns'][$columnIndex]['name']; // Column name
        $columnSortOrder = $_POST['order'][0]['dir']; // asc or desc

        $parameter = array("offset" => $iDisplayStart,'limit'=> $iDisplayLength,'orderBy' => $columnName,"order" => $columnSortOrder);

        $result = $this->getDoctrine()->getRepository('TerminalbdNbrvatBundle:VdsCertificate')->vdsFindWithSearch($config,$parameter,$query);

        $i = $iDisplayStart > 0  ? ($iDisplayStart+1) : 1;

        /* @var $post Item */

        foreach ($result as $post):
            $process = "Done";
            $viewUrl = $this->generateUrl('nbrvat_vds_generate_certificate',array('id' => $post['id']));
            if( $post['process'] == 1){
                $process = "In-progress";
            }
            $created = $post['created']->format('d-m-Y');
            $records["data"][] = array(
                $id                 = $i,
                $created            = $created,
                $invoice            = $post['invoice'],
                $company        = $post['company'],
                $total              = $post['total'],
                $payment            = $post['amount'],
                $balance            = $post['balance'],
                $mode               = ucfirst($post['mode']),
                $process            = $process,
                $action             ="<div><a class='btn btn-mini green-bg white-font' data-action='{$viewUrl}' href='?process=postView-{$post['id']}&check=show#modal' id='postView-{$post['id']}' ><i class='fa fa-eye'></i> View</a></div>");
            $i++;

        endforeach;

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
        return new JsonResponse($records);
    }

    /**
     * @Route("/data-certificate-table", methods={"GET", "POST"}, name="nbrvat_vds_certificate_data_table")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT')")
     */

    public function certificateDataTable(Request $request)
    {

        $query = $_REQUEST;
        $config = $this->getDoctrine()->getRepository(Nbrvat::class)->findConfig($this->getUser());
        $iTotalRecords = $this->getDoctrine()->getRepository(VdsTransaction::class)->rowCount($config);

        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);
        $records = array();
        $records["data"] = array();
        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $columnIndex = $_POST['order'][0]['column']; // Column index
        $columnName = $_POST['columns'][$columnIndex]['name']; // Column name
        $columnSortOrder = $_POST['order'][0]['dir']; // asc or desc

        $parameter = array("offset" => $iDisplayStart,'limit'=> $iDisplayLength,'orderBy' => $columnName,"order" => $columnSortOrder);

        $result = $this->getDoctrine()->getRepository('TerminalbdNbrvatBundle:VdsTransaction')->vdsFindWithSearch($config,$parameter,$query);

        $i = $iDisplayStart > 0  ? ($iDisplayStart+1) : 1;

        foreach ($result as $post):

            $processUrl = "";
            $certificateUrl = "";
            $deleteUrl = "";

            if($post['process'] == "created"){
                $processUrl ="<a class='btn btn-mini blue-bg white-font approve' href='javascript:' data-action='{$this->generateUrl('nbrvat_vds_process',array('id' => $post['id']))}'><i class='fa fa-check'></i> Checked</a>";
                $deleteUrl = "<a class='btn btn-mini transparent-bg red-font remove' href='javascript:' data-action='{$this->generateUrl('nbrvat_vds_delete',array('id'=> $post['id']))}'><i class='fa fa-remove'></i> Remove</a>";
            }elseif($post['process'] == "checked"){
                $processUrl ="<a class='btn btn-mini blue-bg white-font approve' href='javascript:' data-action='{$this->generateUrl('nbrvat_vds_process',array('id' => $post['id']))}'><i class='fa fa-check-square'></i> Approved</a>";
            }elseif($post['process'] == "approved") {
                $certificateUrl = "<a target='_blank' class='btn btn-mini orange-bg white-font' href='{$this->generateUrl('nbrvat_vds_certificate_show',array('id' => $post['id']))}'><i class='fa fa-download'></i> Certificate</a>";
            }

            $created = $post['created']->format('d-m-Y');

            $records["data"][] = array(
                $id                 = $i,
                $created            = $created,
                $invoice            = $post['invoice'],
                $company            = $post['company'],
                $challanNo          = $post['challanNo'],
                $vdsAmount          = $post['total'],
                $balance            = $post['balance'],
                $amount             = $post['amount'],
                $process            = ucfirst($post['process']),
                $action             ="<div>{$processUrl}{$certificateUrl}{$deleteUrl}</div>");
            $i++;

        endforeach;

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
        return new JsonResponse($records);

    }

}
