<?php

namespace Terminalbd\NbrvatBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;



/**
 * TaxTariff
 *
 * @ORM\Table("nbr_input_out_tax")
 * @ORM\Entity(repositoryClass="Terminalbd\NbrvatBundle\Repository\TaxReturnRepository")
 */
class InputOutputTax
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;


    /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\NbrvatBundle\Entity\TaxReturn", inversedBy="supplyOutputTaxes" )
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $taxReturn;


     /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\NbrvatBundle\Entity\Setting")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $outputTax;

    /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\NbrvatBundle\Entity\Setting")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $inputTax;


    /**
     * @var float
     *
     * @ORM\Column(type="float",nullable=true)
     */
    private $totalAmount = 0;

    /**
     * @var float
     *
     * @ORM\Column(type="float",nullable=true)
     */
    private $supplementoryDuty = 0;

    /**
     * @var float
     *
     * @ORM\Column(type="float",nullable=true)
     */
    private $valueAddedTax = 0;


    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="boolean")
     */
    private $status = true;



    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated", type="datetime")
     */
    private $updated;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }



    /**
     * @return Setting
     */
    public function getOutputTax()
    {
        return $this->outputTax;
    }

    /**
     * @param Setting $outputTax
     */
    public function setOutputTax($outputTax)
    {
        $this->outputTax = $outputTax;
    }

    /**
     * @return Setting
     */
    public function getInputTax()
    {
        return $this->inputTax;
    }

    /**
     * @param Setting $inputTax
     */
    public function setInputTax($inputTax)
    {
        $this->inputTax = $inputTax;
    }

    /**
     * @return float
     */
    public function getTotalAmount()
    {
        return $this->totalAmount;
    }

    /**
     * @param float $totalAmount
     */
    public function setTotalAmount(float $totalAmount)
    {
        $this->totalAmount = $totalAmount;
    }

    /**
     * @return float
     */
    public function getSupplementoryDuty()
    {
        return $this->supplementoryDuty;
    }

    /**
     * @param float $supplementoryDuty
     */
    public function setSupplementoryDuty(float $supplementoryDuty)
    {
        $this->supplementoryDuty = $supplementoryDuty;
    }

    /**
     * @return float
     */
    public function getValueAddedTax()
    {
        return $this->valueAddedTax;
    }

    /**
     * @param float $valueAddedTax
     */
    public function setValueAddedTax(float $valueAddedTax)
    {
        $this->valueAddedTax = $valueAddedTax;
    }

    /**
     * @return bool
     */
    public function isStatus()
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus(bool $status)
    {
        $this->status = $status;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated(\DateTime $updated)
    {
        $this->updated = $updated;
    }
    
    


}

