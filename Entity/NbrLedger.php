<?php

namespace Terminalbd\NbrvatBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;


/**
 * NbrLedger
 *
 * @ORM\Table("nbr_ledger")
 * @ORM\Entity(repositoryClass="Terminalbd\NbrvatBundle\Repository\NbrLedgerRepository")
 */
class NbrLedger
{


    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $process;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Application\NbrVat")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private $config;

    /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\NbrvatBundle\Entity\Setting")
     **/
    private $taxNote;


    /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\NbrvatBundle\Entity\TaxReturn")
     **/
    private $taxReturn;


    /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\NbrvatBundle\Entity\NetTaxPayable")
     **/
    private $netTaxPayable;


     /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\NbrvatBundle\Entity\BankTreasury")
     **/
    private $bankTreasury;



    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $remark;


     /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $vatDebit = 0.00;


    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $vatCredit = 0.00;

    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $vatOpening = 0.00;

    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $vatClosing = 0.00;


    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $sdDebit = 0.00;


    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $sdCredit = 0.00;


    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $sdOpening = 0.00;


    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $sdClosing = 0.00;


    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $atDebit = 0.00;


    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $atCredit = 0.00;


    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $atClosing = 0.00;

    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $atOpening = 0.00;


    /**
     * @var boolean
     * @ORM\Column(name="status", type="boolean")
     */
    private $status = true;


    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;


    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated", type="datetime")
     */
    private $updated;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return bool
     */
    public function isStatus()
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }


    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return string
     */
    public function getProcess(): string
    {
        return $this->process;
    }

    /**
     * @param string $process
     */
    public function setProcess(string $process)
    {
        $this->process = $process;
    }




}

