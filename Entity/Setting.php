<?php

namespace Terminalbd\NbrvatBundle\Entity;

use App\Entity\Admin\SettingType;
use App\Entity\Application\Inventory;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * BusinessParticular
 *
 * @ORM\Table( name = "nbr_setting")
 * @ORM\Entity(repositoryClass="Terminalbd\NbrvatBundle\Repository\SettingRepository")
 */
class Setting
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Application\Nbrvat")
     **/
    private $config;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Admin\SettingType", inversedBy="nbrSettings")
     **/
    private $settingType;


    /**
     * @ORM\OneToMany(targetEntity="Terminalbd\InventoryBundle\Entity\Item", mappedBy="productGroup" )
     **/
    private $items;

    /**
     * @ORM\OneToMany(targetEntity="Terminalbd\InventoryBundle\Entity\StockItem", mappedBy="taxReturnNote" )
     **/
    private $stockItem;

    /**
     * @ORM\OneToMany(targetEntity="Terminalbd\NbrvatBundle\Entity\TaxReturn", mappedBy="returnType" )
     **/
    private $taxReturnTypes;

    /**
     * @ORM\OneToMany(targetEntity="Terminalbd\NbrvatBundle\Entity\TaxSetup", mappedBy="tax" )
     **/
    private $taxSetups;


     /**
     * @ORM\OneToMany(targetEntity="Terminalbd\NbrvatBundle\Entity\VatAdjustment", mappedBy="adjument" )
     **/
    private $vatAdjustments;


    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;


    /**
     * @Gedmo\Slug(handlers={
     *      @Gedmo\SlugHandler(class="Gedmo\Sluggable\Handler\TreeSlugHandler", options={
     *          @Gedmo\SlugHandlerOption(name="parentRelationField", value="settingType"),
     *          @Gedmo\SlugHandlerOption(name="separator", value="-")
     *      })
     * }, fields={"name"})
     * @ORM\Column(length=255, unique=true)
     * @Doctrine\ORM\Mapping\Column()
     */
    private $slug;


    /**
     * @var string
     *
     * @ORM\Column(name="nameBn", type="string", length=255, nullable=true)
     */
    private $nameBn;

    /**
     * @var string
     *
     * @ORM\Column(name="shortCode", type="string", length=50, nullable=true)
     */
    private $shortCode;

    /**
     * @var string
     *
     * @ORM\Column(type="string",nullable=true)
     */
    private $accountCode;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer",nullable=true)
     */
    private $noteNo;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=25, nullable=true)
     */
     private $mode;


    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="boolean" )
     */
    private $status= true;


    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true )
     */
    private $subForm = false;


    /**
     * @var boolean
     *
     * @ORM\Column(name="isInput", type="boolean" )
     */
    private $isInput = false;


    /**
     * Get id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * @return bool
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }


    /**
     * @return Item
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @return string
     */
    public function getShortCode()
    {
        return $this->shortCode;
    }

    /**
     * @param string $shortCode
     */
    public function setShortCode($shortCode)
    {
        $this->shortCode = $shortCode;
    }

    /**
     * @return SettingType
     */
    public function getSettingType()
    {
        return $this->settingType;
    }

    /**
     * @param SettingType $settingType
     */
    public function setSettingType($settingType)
    {
        $this->settingType = $settingType;
    }

    /**
     * @return Inventory
     */
    public function getConfig()
    {
        return $this->config();
    }

    /**
     * @param Inventory $settingType
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }

    /**
     * @return string
     */
    public function getNameBn()
    {
        return $this->nameBn;
    }

    /**
     * @param string $nameBn
     */
    public function setNameBn(string $nameBn)
    {
        $this->nameBn = $nameBn;
    }

    /**
     * @return string
     */
    public function getAccountCode()
    {
        return $this->accountCode;
    }

    /**
     * @param string $accountCode
     */
    public function setAccountCode(string $accountCode)
    {
        $this->accountCode = $accountCode;
    }

    public function treasuryAccountCode(){

        $return =  $this->getName()." - ".$this->getAccountCode();

        return $return;

    }

    public function noteWithName(){

        $return =  $this->getNoteNo()." - ".$this->getName();

        return $return;

    }

    /**
     * @return ItemKeyValue
     */
    public function getItemKeyValues()
    {
        return $this->itemKeyValues;
    }

    /**
     * @return mixed
     */
    public function getNoteNo()
    {
        return $this->noteNo;
    }

    /**
     * @param mixed $noteNo
     */
    public function setNoteNo($noteNo)
    {
        $this->noteNo = $noteNo;
    }

    /**
     * @return bool
     */
    public function isInput()
    {
        return $this->isInput;
    }

    /**
     * @param bool $isInput
     */
    public function setIsInput(bool $isInput)
    {
        $this->isInput = $isInput;
    }

    /**
     * @return string
     */
    public function getMode()
    {
        return $this->mode;
    }

    /**
     * @param string $mode
     */
    public function setMode($mode)
    {
        $this->mode = $mode;
    }

    /**
     * @return TaxSetup
     */
    public function getTaxSetups()
    {
        return $this->taxSetups;
    }

    /**
     * @return bool
     */
    public function isSubForm()
    {
        return $this->subForm;
    }

    /**
     * @param bool $subForm
     */
    public function setSubForm(bool $subForm)
    {
        $this->subForm = $subForm;
    }



}

