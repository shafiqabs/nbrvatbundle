<?php

namespace Terminalbd\NbrvatBundle\Entity;

use App\Entity\Admin\Terminal;
use App\Entity\Application\Nbrvat;
use App\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;



/**
 * TaxTariff
 *
 * @ORM\Table("nbr_tax_return")
 * @ORM\Entity(repositoryClass="Terminalbd\NbrvatBundle\Repository\TaxReturnRepository")
 */
class TaxReturn
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Application\Nbrvat")
     **/
    private $config;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Admin\Terminal", inversedBy="taxReturn")
     **/
    private  $terminal;
    

    /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\NbrvatBundle\Entity\Setting", inversedBy="taxReturnTypes" )
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $returnType;

    /**
     * @ORM\OneToMany(targetEntity="Terminalbd\NbrvatBundle\Entity\VatAdjustment", mappedBy="taxreturn" )
     **/
    private  $vatAdjustment;


    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=false)
     */
    private $activitiesTaxPeriod = false;


    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=false)
     */
    private $process = "created";

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $taxPeriod;


    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $outputValue;

     /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $outputSd;

     /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $outputVat;

    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $inputValue;

    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $inputVat;


    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $inputSd;


    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $incresingAdjustmentPart5;


    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $decresingAdjustmentPart6;


    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $advanceTaxImportVatPart7;

     /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $netPayableVatPart7;

    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $netPayableVatAdjustmentPart7;

    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $netPayableSdBeforePart7;

    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $netPayableSdAfterPart7;

    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $sdAgainstDebitNotePart7;


    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $sdAgainstCreditNotePart7;


    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $sdAgainstExportPart7;


    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $netPayableVatTreasuryPart7;


    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $netPayableSdTreasuryPart7;


    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $lastTaxPeriodVatPart7;

    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $lastTaxPeriodSdPart7;


    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $oldDecresingAdjustmentVatPart8;

    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $oldDecresingAdjustmentSdPart8;


    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $oldRemainingVatBalancePart8;

    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $oldRemainingSDBalancePart8;

    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $vatDepositeTreasuryPart9;

    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $sdDepositeTreasuryPart9;

    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $closingBalanceVatPart10;

    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $closingBalanceSdPart10;
    

    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $refundVatPart11;

    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $refundSdPart11;


    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     */
    private $isRefund = false;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     */
    private $refundConfirm = false;


    /**
     * @var date
     *
     * @ORM\Column(type="date", nullable=true)
     */
    private $taxMonth;


    /**
     * @var string
     * @ORM\Column(type="string",nullable = true )
     */
    private $signaturePerson;

    /**
     * @var string
     * @ORM\Column(type="string",nullable = true )
     */
    private $signatureDesignation;


    /**
     * @var string
     * @ORM\Column(type="string",nullable = true )
     */
    private $signatureMobile;

    /**
     * @var string
     * @ORM\Column(type="string", nullable = true )
     */
    private $signatureEmail;

    /**
     * @var string
     * @ORM\Column(type="string",nullable = true )
     */
    private $signatureNid;


    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="boolean")
     */
    private $status = true;


    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     **/
    private  $createdBy;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     **/
    private  $checkedBy;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     **/
    private  $approvedBy;

    /**
     * @var \DateTime
     * @ORM\Column(name="submissionDate", type="datetime", nullable=true)
     */
    private $submissionDate;

    /**
     * @var \DateTimee
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated", type="datetime")
     */
    private $updated;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Nbrvat
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param Nbrvat $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }

    /**
     * @return Terminal
     */
    public function getTerminal()
    {
        return $this->terminal;
    }

    /**
     * @param Terminal $terminal
     */
    public function setTerminal($terminal)
    {
        $this->terminal = $terminal;
    }

    /**
     * @return bool
     */
    public function isActivitiesTaxPeriod()
    {
        return $this->activitiesTaxPeriod;
    }

    /**
     * @param bool $activitiesTaxPeriod
     */
    public function setActivitiesTaxPeriod(bool $activitiesTaxPeriod)
    {
        $this->activitiesTaxPeriod = $activitiesTaxPeriod;
    }

    /**
     * @return bool
     */
    public function isStatus()
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus(bool $status)
    {
        $this->status = $status;
    }

    /**
     * @return User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param User $createdBy
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    }

    /**
     * @return User
     */
    public function getCheckedBy()
    {
        return $this->checkedBy;
    }

    /**
     * @param User $checkedBy
     */
    public function setCheckedBy($checkedBy)
    {
        $this->checkedBy = $checkedBy;
    }

    /**
     * @return User
     */
    public function getApprovedBy()
    {
        return $this->approvedBy;
    }

    /**
     * @param User $approvedBy
     */
    public function setApprovedBy($approvedBy)
    {
        $this->approvedBy = $approvedBy;
    }

    /**
     * @return \DateTime
     */
    public function getSubmissionDate()
    {
        return $this->submissionDate;
    }

    /**
     * @param \DateTime $submissionDate
     */
    public function setSubmissionDate(\DateTime $submissionDate)
    {
        $this->submissionDate = $submissionDate;
    }

    /**
     * @return \DateTimee
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTimee $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated(\DateTime $updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return Setting
     */
    public function getReturnType()
    {
        return $this->returnType;
    }

    /**
     * @param Setting $returnType
     */
    public function setReturnType($returnType)
    {
        $this->returnType = $returnType;
    }

    /**
     * @return string
     */
    public function getProcess(): string
    {
        return $this->process;
    }

    /**
     * @param string $process
     */
    public function setProcess(string $process)
    {
        $this->process = $process;
    }

    /**
     * @return string
     */
    public function getTaxPeriod()
    {
        return $this->taxPeriod;
    }

    /**
     * @param string $taxPeriod
     */
    public function setTaxPeriod($taxPeriod)
    {
        $this->taxPeriod = $taxPeriod;
    }

    /**
     * @return string
     */
    public function getSignaturePerson()
    {
        return $this->signaturePerson;
    }

    /**
     * @param string $signaturePerson
     */
    public function setSignaturePerson($signaturePerson)
    {
        $this->signaturePerson = $signaturePerson;
    }

    /**
     * @return string
     */
    public function getSignatureDesignation()
    {
        return $this->signatureDesignation;
    }

    /**
     * @param string $signatureDesignation
     */
    public function setSignatureDesignation($signatureDesignation)
    {
        $this->signatureDesignation = $signatureDesignation;
    }

    /**
     * @return string
     */
    public function getSignatureMobile()
    {
        return $this->signatureMobile;
    }

    /**
     * @param string $signatureMobile
     */
    public function setSignatureMobile($signatureMobile)
    {
        $this->signatureMobile = $signatureMobile;
    }

    /**
     * @return string
     */
    public function getSignatureEmail()
    {
        return $this->signatureEmail;
    }

    /**
     * @param string $signatureEmail
     */
    public function setSignatureEmail($signatureEmail)
    {
        $this->signatureEmail = $signatureEmail;
    }

    /**
     * @return string
     */
    public function getSignatureNid()
    {
        return $this->signatureNid;
    }

    /**
     * @param string $signatureNid
     */
    public function setSignatureNid($signatureNid)
    {
        $this->signatureNid = $signatureNid;
    }

    /**
     * @return date
     */
    public function getTaxMonth()
    {
        return $this->taxMonth;
    }

    /**
     * @param date $taxMonth
     */
    public function setTaxMonth($taxMonth)
    {
        $this->taxMonth = $taxMonth;
    }

    /**
     * @return float
     */
    public function getOutputValue()
    {
        return $this->outputValue;
    }

    /**
     * @param float $outputValue
     */
    public function setOutputValue($outputValue)
    {
        $this->outputValue = $outputValue;
    }

    /**
     * @return float
     */
    public function getOutputSd()
    {
        return $this->outputSd;
    }

    /**
     * @param float $outputSd
     */
    public function setOutputSd( $outputSd)
    {
        $this->outputSd = $outputSd;
    }

    /**
     * @return float
     */
    public function getOutputVat()
    {
        return $this->outputVat;
    }

    /**
     * @param float $outputVat
     */
    public function setOutputVat( $outputVat)
    {
        $this->outputVat = $outputVat;
    }

    /**
     * @return float
     */
    public function getInputValue()
    {
        return $this->inputValue;
    }

    /**
     * @param float $inputValue
     */
    public function setInputValue( $inputValue)
    {
        $this->inputValue = $inputValue;
    }

    /**
     * @return float
     */
    public function getInputVat()
    {
        return $this->inputVat;
    }

    /**
     * @param float $inputVat
     */
    public function setInputVat( $inputVat)
    {
        $this->inputVat = $inputVat;
    }

    /**
     * @return float
     */
    public function getInputSd()
    {
        return $this->inputSd;
    }

    /**
     * @param float $inputSd
     */
    public function setInputSd( $inputSd)
    {
        $this->inputSd = $inputSd;
    }

    /**
     * @return float
     */
    public function getIncresingAdjustmentPart5()
    {
        return $this->incresingAdjustmentPart5;
    }

    /**
     * @param float $incresingAdjustmentPart5
     */
    public function setIncresingAdjustmentPart5( $incresingAdjustmentPart5)
    {
        $this->incresingAdjustmentPart5 = $incresingAdjustmentPart5;
    }

    /**
     * @return float
     */
    public function getDecresingAdjustmentPart6()
    {
        return $this->decresingAdjustmentPart6;
    }

    /**
     * @param float $decresingAdjustmentPart6
     */
    public function setDecresingAdjustmentPart6( $decresingAdjustmentPart6)
    {
        $this->decresingAdjustmentPart6 = $decresingAdjustmentPart6;
    }

    /**
     * @return float
     */
    public function getNetPayableVatPart7()
    {
        return $this->netPayableVatPart7;
    }

    /**
     * @param float $netPayableVatPart7
     */
    public function setNetPayableVatPart7( $netPayableVatPart7)
    {
        $this->netPayableVatPart7 = $netPayableVatPart7;
    }

    /**
     * @return float
     */
    public function getNetPayableVatAdjustmentPart7()
    {
        return $this->netPayableVatAdjustmentPart7;
    }

    /**
     * @param float $netPayableVatAdjustmentPart7
     */
    public function setNetPayableVatAdjustmentPart7( $netPayableVatAdjustmentPart7)
    {
        $this->netPayableVatAdjustmentPart7 = $netPayableVatAdjustmentPart7;
    }

    /**
     * @return float
     */
    public function getNetPayableSdBeforePart7()
    {
        return $this->netPayableSdBeforePart7;
    }

    /**
     * @param float $netPayableSdBeforePart7
     */
    public function setNetPayableSdBeforePart7( $netPayableSdBeforePart7)
    {
        $this->netPayableSdBeforePart7 = $netPayableSdBeforePart7;
    }

    /**
     * @return float
     */
    public function getNetPayableSdAfterPart7()
    {
        return $this->netPayableSdAfterPart7;
    }

    /**
     * @param float $netPayableSdAfterPart7
     */
    public function setNetPayableSdAfterPart7( $netPayableSdAfterPart7)
    {
        $this->netPayableSdAfterPart7 = $netPayableSdAfterPart7;
    }

    /**
     * @return float
     */
    public function getSdAgainstDebitNotePart7()
    {
        return $this->sdAgainstDebitNotePart7;
    }

    /**
     * @param float $sdAgainstDebitNotePart7
     */
    public function setSdAgainstDebitNotePart7( $sdAgainstDebitNotePart7)
    {
        $this->sdAgainstDebitNotePart7 = $sdAgainstDebitNotePart7;
    }

    /**
     * @return float
     */
    public function getSdAgainstCreditNotePart7()
    {
        return $this->sdAgainstCreditNotePart7;
    }

    /**
     * @param float $sdAgainstCreditNotePart7
     */
    public function setSdAgainstCreditNotePart7( $sdAgainstCreditNotePart7)
    {
        $this->sdAgainstCreditNotePart7 = $sdAgainstCreditNotePart7;
    }

    /**
     * @return float
     */
    public function getNetPayableVatTreasuryPart7()
    {
        return $this->netPayableVatTreasuryPart7;
    }

    /**
     * @param float $netPayableVatTreasuryPart7
     */
    public function setNetPayableVatTreasuryPart7( $netPayableVatTreasuryPart7)
    {
        $this->netPayableVatTreasuryPart7 = $netPayableVatTreasuryPart7;
    }

    /**
     * @return float
     */
    public function getNetPayableSdTreasuryPart7()
    {
        return $this->netPayableSdTreasuryPart7;
    }

    /**
     * @param float $netPayableSdTreasuryPart7
     */
    public function setNetPayableSdTreasuryPart7( $netPayableSdTreasuryPart7)
    {
        $this->netPayableSdTreasuryPart7 = $netPayableSdTreasuryPart7;
    }

    /**
     * @return float
     */
    public function getLastTaxPeriodVatPart7()
    {
        return $this->lastTaxPeriodVatPart7;
    }

    /**
     * @param float $lastTaxPeriodVatPart7
     */
    public function setLastTaxPeriodVatPart7( $lastTaxPeriodVatPart7)
    {
        $this->lastTaxPeriodVatPart7 = $lastTaxPeriodVatPart7;
    }

    /**
     * @return float
     */
    public function getLastTaxPeriodSdPart7()
    {
        return $this->lastTaxPeriodSdPart7;
    }

    /**
     * @param float $lastTaxPeriodSdPart7
     */
    public function setLastTaxPeriodSdPart7( $lastTaxPeriodSdPart7)
    {
        $this->lastTaxPeriodSdPart7 = $lastTaxPeriodSdPart7;
    }

    /**
     * @return float
     */
    public function getOldDecresingAdjustmentVatPart8()
    {
        return $this->oldDecresingAdjustmentVatPart8;
    }

    /**
     * @param float $oldDecresingAdjustmentVatPart8
     */
    public function setOldDecresingAdjustmentVatPart8( $oldDecresingAdjustmentVatPart8)
    {
        $this->oldDecresingAdjustmentVatPart8 = $oldDecresingAdjustmentVatPart8;
    }

    /**
     * @return float
     */
    public function getOldDecresingAdjustmentSdPart8()
    {
        return $this->oldDecresingAdjustmentSdPart8;
    }

    /**
     * @param float $oldDecresingAdjustmentSdPart8
     */
    public function setOldDecresingAdjustmentSdPart8( $oldDecresingAdjustmentSdPart8)
    {
        $this->oldDecresingAdjustmentSdPart8 = $oldDecresingAdjustmentSdPart8;
    }

    /**
     * @return float
     */
    public function getOldRemainingVatBalancePart8()
    {
        return $this->oldRemainingVatBalancePart8;
    }

    /**
     * @param float $oldRemainingVatBalancePart8
     */
    public function setOldRemainingVatBalancePart8( $oldRemainingVatBalancePart8)
    {
        $this->oldRemainingVatBalancePart8 = $oldRemainingVatBalancePart8;
    }

    /**
     * @return float
     */
    public function getOldRemainingSDBalancePart8()
    {
        return $this->oldRemainingSDBalancePart8;
    }

    /**
     * @param float $oldRemainingSDBalancePart8
     */
    public function setOldRemainingSDBalancePart8( $oldRemainingSDBalancePart8)
    {
        $this->oldRemainingSDBalancePart8 = $oldRemainingSDBalancePart8;
    }

    /**
     * @return float
     */
    public function getClosingBalanceVatPart10()
    {
        return $this->closingBalanceVatPart10;
    }

    /**
     * @param float $closingBalanceVatPart10
     */
    public function setClosingBalanceVatPart10( $closingBalanceVatPart10)
    {
        $this->closingBalanceVatPart10 = $closingBalanceVatPart10;
    }

    /**
     * @return float
     */
    public function getClosingBalanceSdPart10()
    {
        return $this->closingBalanceSdPart10;
    }

    /**
     * @param float $closingBalanceSdPart10
     */
    public function setClosingBalanceSdPart10( $closingBalanceSdPart10)
    {
        $this->closingBalanceSdPart10 = $closingBalanceSdPart10;
    }

    /**
     * @return float
     */
    public function getRefundVatPart11()
    {
        return $this->refundVatPart11;
    }

    /**
     * @param float $refundVatPart11
     */
    public function setRefundVatPart11( $refundVatPart11)
    {
        $this->refundVatPart11 = $refundVatPart11;
    }

    /**
     * @return float
     */
    public function getRefundSdPart11()
    {
        return $this->refundSdPart11;
    }

    /**
     * @param float $refundSdPart11
     */
    public function setRefundSdPart11( $refundSdPart11)
    {
        $this->refundSdPart11 = $refundSdPart11;
    }

    /**
     * @return bool
     */
    public function isRefund()
    {
        return $this->isRefund;
    }

    /**
     * @param bool $isRefund
     */
    public function setIsRefund(bool $isRefund)
    {
        $this->isRefund = $isRefund;
    }

    /**
     * @return bool
     */
    public function isRefundConfirm()
    {
        return $this->refundConfirm;
    }

    /**
     * @param bool $refundConfirm
     */
    public function setRefundConfirm(bool $refundConfirm)
    {
        $this->refundConfirm = $refundConfirm;
    }

    /**
     * @return float
     */
    public function getSdAgainstExportPart7()
    {
        return $this->sdAgainstExportPart7;
    }

    /**
     * @param float $sdAgainstExportPart7
     */
    public function setSdAgainstExportPart7( $sdAgainstExportPart7)
    {
        $this->sdAgainstExportPart7 = $sdAgainstExportPart7;
    }

    /**
     * @return float
     */
    public function getVatDepositeTreasuryPart9()
    {
        return $this->vatDepositeTreasuryPart9;
    }

    /**
     * @param float $vatDepositeTreasuryPart9
     */
    public function setVatDepositeTreasuryPart9( $vatDepositeTreasuryPart9)
    {
        $this->vatDepositeTreasuryPart9 = $vatDepositeTreasuryPart9;
    }

    /**
     * @return float
     */
    public function getSdDepositeTreasuryPart9()
    {
        return $this->sdDepositeTreasuryPart9;
    }

    /**
     * @param float $sdDepositeTreasuryPart9
     */
    public function setSdDepositeTreasuryPart9( $sdDepositeTreasuryPart9)
    {
        $this->sdDepositeTreasuryPart9 = $sdDepositeTreasuryPart9;
    }

    /**
     * @return VatAdjustment
     */
    public function getVatAdjustment()
    {
        return $this->vatAdjustment;
    }

    /**
     * @return float
     */
    public function getAdvanceTaxImportVatPart7()
    {
        return $this->advanceTaxImportVatPart7;
    }

    /**
     * @param float $advanceTaxImportVatPart7
     */
    public function setAdvanceTaxImportVatPart7($advanceTaxImportVatPart7)
    {
        $this->advanceTaxImportVatPart7 = $advanceTaxImportVatPart7;
    }





}

