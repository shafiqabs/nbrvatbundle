<?php

namespace Terminalbd\NbrvatBundle\Entity;

use App\Entity\Admin\SettingType;
use App\Entity\Application\Inventory;
use App\Entity\Application\Nbrvat;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * BusinessParticular
 *
 * @ORM\Table( name = "nbr_taxsetup")
 * @ORM\Entity(repositoryClass="Terminalbd\NbrvatBundle\Repository\TaxSetupRepository")
 */
class TaxSetup
{
    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Application\Nbrvat", inversedBy="taxSetups")
     **/
    private $config;


    /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\NbrvatBundle\Entity\Setting", inversedBy="taxSetups")
     **/
    private $tax;


    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $inputMode = "dropdown";

    /**
     * @var string
     *
     * @ORM\Column(type="text",nullable=true)
     */
    private $inputValue ="0|0";


     /**
     * @var string
     *
     * @ORM\Column(type="string",nullable=true)
     */
    private $taxMode = "percent";


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Nbrvat
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param Nbrvat $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }

    /**
     * @return Setting
     */
    public function getTax()
    {
        return $this->tax;
    }

    /**
     * @param Setting $tax
     */
    public function setTax($tax)
    {
        $this->tax = $tax;
    }

    /**
     * @return string
     */
    public function getInputMode()
    {
        return $this->inputMode;
    }

    /**
     * @param string $inputMode
     */
    public function setInputMode(string $inputMode)
    {
        $this->inputMode = $inputMode;
    }

    /**
     * @return string
     */
    public function getInputValue()
    {
        return $this->inputValue;
    }

    /**
     * @param string $inputValue
     */
    public function setInputValue(string $inputValue)
    {
        $this->inputValue = $inputValue;
    }

    /**
     * @return string
     */
    public function getTaxMode()
    {
        return $this->taxMode;
    }

    /**
     * @param string $taxMode
     */
    public function setTaxMode(string $taxMode)
    {
        $this->taxMode = $taxMode;
    }

}

