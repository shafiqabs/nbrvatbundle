<?php

namespace Terminalbd\NbrvatBundle\Entity;

use App\Entity\Admin\Bank;
use App\Entity\Application\Nbrvat;
use App\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Terminalbd\NbrvatBundle\Entity\Setting;


/**
 * TaxTariff
 *
 * @ORM\Table("nbr_adjustment")
 * @ORM\Entity(repositoryClass="Terminalbd\NbrvatBundle\Repository\VatAdjustmentRepository")
 */
class VatAdjustment
{

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Application\Nbrvat", inversedBy="vatAdjustment")
     **/
    private $config;

    /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\NbrvatBundle\Entity\Setting", inversedBy="vatAdjustment")
     **/
    private  $adjument;

    /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\NbrvatBundle\Entity\TaxReturn", inversedBy="vatAdjustment")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $taxreturn;


     /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true, length=50)
     */
    private $mode;

     /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true, length=50)
     */
    private $process = "created";

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $amount;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $remark;


    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="boolean")
     */
    private $status = true;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $issueDate;

    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     **/
    private  $createdBy;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     **/
    private  $checkedBy;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     **/
    private  $approvedBy;


    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated", type="datetime")
     */
    private $updated;


    /**
     * Get id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return Nbrvat
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param Nbrvat $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return string
     */
    public function getRemark()
    {
        return $this->remark;
    }

    /**
     * @param string $remark
     */
    public function setRemark(string $remark)
    {
        $this->remark = $remark;
    }

    /**
     * @return bool
     */
    public function isStatus(): ? bool
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus(bool $status)
    {
        $this->status = $status;
    }


    /**
     * @return string
     */
    public function getProcess()
    {
        return $this->process;
    }

    /**
     * @param string $process
     */
    public function setProcess(string $process)
    {
        $this->process = $process;
    }

    /**
     * @return \DateTime
     */
    public function getIssueDate(): ? \DateTime
    {
        return $this->issueDate;
    }

    /**
     * @param \DateTime $issueDate
     */
    public function setIssueDate(\DateTime $issueDate)
    {
        $this->issueDate = $issueDate;
    }

    /**
     * @return User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param User $createdBy
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    }

    /**
     * @return User
     */
    public function getCheckedBy()
    {
        return $this->checkedBy;
    }

    /**
     * @param User $checkedBy
     * @return BankTreasury
     */
    public function setCheckedBy($checkedBy)
    {
        $this->checkedBy = $checkedBy;
        return $this;
    }

    /**
     * @return User
     */
    public function getApprovedBy()
    {
        return $this->approvedBy;
    }

    /**
     * @param User $approvedBy
     * @return BankTreasury
     */
    public function setApprovedBy($approvedBy)
    {
        $this->approvedBy = $approvedBy;
        return $this;
    }

    /**
     * @return Setting
     */
    public function getAdjument()
    {
        return $this->adjument;
    }

    /**
     * @param Setting $adjument
     */
    public function setAdjument($adjument)
    {
        $this->adjument = $adjument;
    }

    /**
     * @return mixed
     */
    public function getMode()
    {
        return $this->mode;
    }

    /**
     * @param mixed $mode
     */
    public function setMode($mode)
    {
        $this->mode = $mode;
    }

    /**
     * @return TaxReturn
     */
    public function getTaxreturn()
    {
        return $this->taxreturn;
    }

    /**
     * @param TaxReturn $taxreturn
     */
    public function setTaxreturn($taxreturn)
    {
        $this->taxreturn = $taxreturn;
    }



}

