<?php

namespace Terminalbd\NbrvatBundle\Entity;

use App\Entity\Application\Nbrvat;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;


/**
 * NbrLedger
 *
 * @ORM\Table("nbr_tax_old_ledger")
 * @ORM\Entity(repositoryClass="Terminalbd\NbrvatBundle\Repository\VatOldLedgerRepository")
 */
class VatOldLedger
{

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Application\NbrVat")
     **/
    private $config;


    /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\NbrvatBundle\Entity\Setting")
     **/
    private $vatHead;

    /**
     * @ORM\OneToOne(targetEntity="Terminalbd\NbrvatBundle\Entity\NetTaxPayable")
     **/
    private $taxPayable;


    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $process;


    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $remark;


     /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $vatDebit = 0.00;


    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $vatCredit = 0.00;

    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $vatOpening = 0.00;

    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $vatClosing = 0.00;


    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $sdDebit = 0.00;


    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $sdCredit = 0.00;


    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $sdOpening = 0.00;


    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $sdClosing = 0.00;


    /**
     * @var boolean
     * @ORM\Column(name="status", type="boolean")
     */
    private $status = true;


    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;


    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated", type="datetime")
     */
    private $updated;


    /**
     * Get id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return bool
     */
    public function isStatus()
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }


    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return Nbrvat
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param Nbrvat $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }

    /**
     * @return mixed
     */
    public function getVatHead()
    {
        return $this->vatHead;
    }

    /**
     * @param mixed $vatHead
     */
    public function setVatHead($vatHead)
    {
        $this->vatHead = $vatHead;
    }

    /**
     * @return string
     */
    public function getProcess(): string
    {
        return $this->process;
    }

    /**
     * @param string $process
     */
    public function setProcess(string $process)
    {
        $this->process = $process;
    }

    /**
     * @return NetTaxPayable
     */
    public function getTaxPayable()
    {
        return $this->taxPayable;
    }

    /**
     * @param NetTaxPayable $taxPayable
     */
    public function setTaxPayable($taxPayable)
    {
        $this->taxPayable = $taxPayable;
    }

    /**
     * @return string
     */
    public function getRemark()
    {
        return $this->remark;
    }

    /**
     * @param string $remark
     */
    public function setRemark($remark)
    {
        $this->remark = $remark;
    }

    /**
     * @return float
     */
    public function getVatDebit()
    {
        return $this->vatDebit;
    }

    /**
     * @param float $vatDebit
     */
    public function setVatDebit(float $vatDebit)
    {
        $this->vatDebit = $vatDebit;
    }

    /**
     * @return float
     */
    public function getVatCredit()
    {
        return $this->vatCredit;
    }

    /**
     * @param float $vatCredit
     */
    public function setVatCredit(float $vatCredit)
    {
        $this->vatCredit = $vatCredit;
    }

    /**
     * @return float
     */
    public function getVatOpening()
    {
        return $this->vatOpening;
    }

    /**
     * @param float $vatOpening
     */
    public function setVatOpening(float $vatOpening)
    {
        $this->vatOpening = $vatOpening;
    }

    /**
     * @return float
     */
    public function getVatClosing()
    {
        return $this->vatClosing;
    }

    /**
     * @param float $vatClosing
     */
    public function setVatClosing(float $vatClosing)
    {
        $this->vatClosing = $vatClosing;
    }

    /**
     * @return float
     */
    public function getSdDebit()
    {
        return $this->sdDebit;
    }

    /**
     * @param float $sdDebit
     */
    public function setSdDebit(float $sdDebit)
    {
        $this->sdDebit = $sdDebit;
    }

    /**
     * @return float
     */
    public function getSdCredit()
    {
        return $this->sdCredit;
    }

    /**
     * @param float $sdCredit
     */
    public function setSdCredit(float $sdCredit)
    {
        $this->sdCredit = $sdCredit;
    }

    /**
     * @return float
     */
    public function getSdOpening()
    {
        return $this->sdOpening;
    }

    /**
     * @param float $sdOpening
     */
    public function setSdOpening(float $sdOpening)
    {
        $this->sdOpening = $sdOpening;
    }

    /**
     * @return float
     */
    public function getSdClosing()
    {
        return $this->sdClosing;
    }

    /**
     * @param float $sdClosing
     */
    public function setSdClosing(float $sdClosing)
    {
        $this->sdClosing = $sdClosing;
    }




}

