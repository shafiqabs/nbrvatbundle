<?php

namespace Terminalbd\NbrvatBundle\Entity;

use App\Entity\Admin\Bank;
use App\Entity\Application\Nbrvat;
use App\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Terminalbd\InventoryBundle\Entity\Purchase;
use Terminalbd\InventoryBundle\Entity\Sales;
use Terminalbd\NbrvatBundle\Entity\Setting;


/**
 * TaxTariff
 *
 * @ORM\Table("nbr_vds_certificate")
 * @ORM\Entity(repositoryClass="Terminalbd\NbrvatBundle\Repository\VdsCertificateRepository")
 */
class VdsCertificate
{

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Application\Nbrvat")
     **/
    private $config;

    /**
     * @ORM\OneToOne(targetEntity="Terminalbd\InventoryBundle\Entity\Sales", inversedBy="vdsCertificate")
     **/
    private  $sales;

    /**
     * @ORM\OneToOne(targetEntity="Terminalbd\InventoryBundle\Entity\Purchase", inversedBy="vdsCertifacte")
     **/
    private  $purchase;


     /**
     * @ORM\OneToMany(targetEntity="Terminalbd\NbrvatBundle\Entity\VdsTransaction", mappedBy="vdsCertificate")
     **/
    private  $vdsTransactions;


    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $invoice;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $company;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $total;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $amount;


     /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $balance;


    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $mode;


    /**
     * @var boolean
     *
     * @ORM\Column(name="isClose", type="boolean")
     */
    private $isClose = true;


    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated", type="datetime")
     */
    private $updated;


    /**
     * Get id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return Nbrvat
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param Nbrvat $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }

    /**
     * @return Sales
     */
    public function getSales()
    {
        return $this->sales;
    }

    /**
     * @param Sales $sales
     */
    public function setSales($sales)
    {
        $this->sales = $sales;
    }

    /**
     * @return Purchase
     */
    public function getPurchase()
    {
        return $this->purchase;
    }

    /**
     * @param Purchase $purchase
     */
    public function setPurchase($purchase)
    {
        $this->purchase = $purchase;
    }

    /**
     * @return float
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param float $total
     */
    public function setTotal(float $total)
    {
        $this->total = $total;
    }

    /**
     * @return float
     */
    public function getReceive()
    {
        return $this->receive;
    }

    /**
     * @param float $receive
     */
    public function setReceive(float $receive)
    {
        $this->receive = $receive;
    }

    /**
     * @return float
     */
    public function getPayment()
    {
        return $this->payment;
    }

    /**
     * @param float $payment
     */
    public function setPayment(float $payment)
    {
        $this->payment = $payment;
    }

    /**
     * @return float
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * @param float $balance
     */
    public function setBalance(float $balance)
    {
        $this->balance = $balance;
    }

    /**
     * @return bool
     */
    public function isClose()
    {
        return $this->isClose;
    }

    /**
     * @param bool $isClose
     */
    public function setIsClose(bool $isClose)
    {
        $this->isClose = $isClose;
    }

    /**
     * @return VdsTransaction
     */
    public function getVdsTransactions()
    {
        return $this->vdsTransactions;
    }

    /**
     * @return string
     */
    public function getMode()
    {
        return $this->mode;
    }

    /**
     * @param string $mode
     */
    public function setMode($mode)
    {
        $this->mode = $mode;
    }

    /**
     * @return string
     */
    public function getInvoice()
    {
        return $this->invoice;
    }

    /**
     * @param string $invoice
     */
    public function setInvoice($invoice)
    {
        $this->invoice = $invoice;
    }

    /**
     * @return string
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param string $company
     */
    public function setCompany($company)
    {
        $this->company = $company;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount( $amount)
    {
        $this->amount = $amount;
    }

    public function getVdsPurchaseInvoice()
    {
        if($this->getPurchase()){
            return $this->getPurchase()->getChallanNo().' - '.$this->getPurchase()->getVendor()->getCompanyName()." (". $this->getPurchase()->getInvoice().")";
        }
        return false;

    }

    public function getVdsSalesInvoice()
    {
        if($this->getSales()){
            return $this->getSales()->getCustomer()->getCompanyName()." (". $this->getSales()->getInvoice().")";
        }
        return false;
    }


}

