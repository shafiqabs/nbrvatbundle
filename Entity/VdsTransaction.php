<?php

namespace Terminalbd\NbrvatBundle\Entity;

use App\Entity\Admin\Bank;
use App\Entity\Application\Nbrvat;
use App\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Terminalbd\NbrvatBundle\Entity\Setting;


/**
 * TaxTariff
 *
 * @ORM\Table("nbr_vds_transaction")
 * @ORM\Entity(repositoryClass="Terminalbd\NbrvatBundle\Repository\VdsTransactionRepository")
 */
class VdsTransaction
{

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Application\Nbrvat")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private $config;


    /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\NbrvatBundle\Entity\VdsCertificate", inversedBy="vdsTransactions")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $vdsCertificate;

    /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\NbrvatBundle\Entity\Setting", inversedBy="bankTreasury")
     **/
    private  $treasuaryCode;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Admin\Location")
     **/
    private  $district;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Admin\Bank")
     **/
    private $bank;


    /**
     * @var string
     *
     * @ORM\Column(name="bankBranch", type="string", nullable=true)
     */
    private $bankBranch;


    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $treasuryChallanNo;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $accountCode;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $address;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $issueDate;

    /**
     * @var string
     *
     * @ORM\Column(name="issuePerson", type="string", length = 100, nullable=true)
     */
    private $issuePerson;

    /**
     * @var string
     *
     * @ORM\Column(name="issueDesignation", type="string", length = 50, nullable=true)
     */
    private $issueDesignation;


    /**
     * @var string
     *
     * @ORM\Column(name="issueAddress", type="string", length = 50, nullable=true)
     */
    private $issueAddress;


    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true, length=50)
     */
    private $process = "created";

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $amount;

     /**
     * @var float
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $code;

     /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $invoice;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $remark;


    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="boolean")
     */
    private $status = true;



    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     **/
    private  $createdBy;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     **/
    private  $checkedBy;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     **/
    private  $approvedBy;



    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated", type="datetime")
     */
    private $updated;


    /**
     * Get id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }


    /**
     * @return string
     */
    public function getProcess()
    {
        return $this->process;
    }

    /**
     * @param string $process
     */
    public function setProcess(string $process)
    {
        $this->process = $process;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount(float $amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return string
     */
    public function getRemark()
    {
        return $this->remark;
    }

    /**
     * @param string $remark
     */
    public function setRemark(string $remark)
    {
        $this->remark = $remark;
    }

    /**
     * @return bool
     */
    public function isStatus()
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus(bool $status)
    {
        $this->status = $status;
    }

    /**
     * @return \DateTime
     */
    public function getIssueDate()
    {
        return $this->issueDate;
    }

    /**
     * @param \DateTime $issueDate
     */
    public function setIssueDate($issueDate)
    {
        $this->issueDate = $issueDate;
    }

    /**
     * @return User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param User $createdBy
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    }

    /**
     * @return User
     */
    public function getCheckedBy()
    {
        return $this->checkedBy;
    }

    /**
     * @param User $checkedBy
     */
    public function setCheckedBy($checkedBy)
    {
        $this->checkedBy = $checkedBy;
    }

    /**
     * @return User
     */
    public function getApprovedBy()
    {
        return $this->approvedBy;
    }

    /**
     * @param User $approvedBy
     */
    public function setApprovedBy($approvedBy)
    {
        $this->approvedBy = $approvedBy;
    }

    /**
     * @return Nbrvat
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param Nbrvat $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }

    /**
     * @return float
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param float $code
     */
    public function setCode(float $code)
    {
        $this->code = $code;
    }

    /**
     * @return VdsCertificate
     */
    public function getVdsCertificate()
    {
        return $this->vdsCertificate;
    }

    /**
     * @param VdsCertificate $vdsCertificate
     */
    public function setVdsCertificate($vdsCertificate)
    {
        $this->vdsCertificate = $vdsCertificate;
    }

    /**
     * @return string
     */
    public function getInvoice()
    {
        return $this->invoice;
    }

    /**
     * @param string $invoice
     */
    public function setInvoice(string $invoice)
    {
        $this->invoice = $invoice;
    }

    /**
     * @return Bank
     */
    public function getBank()
    {
        return $this->bank;
    }

    /**
     * @param Bank $bank
     */
    public function setBank($bank)
    {
        $this->bank = $bank;
    }

    /**
     * @return string
     */
    public function getBankBranch(): ? string
    {
        return $this->bankBranch;
    }

    /**
     * @param string $bankBranch
     */
    public function setBankBranch(string $bankBranch)
    {
        $this->bankBranch = $bankBranch;
    }

    /**
     * @return string
     */
    public function getTreasuryChallanNo(): ? string
    {
        return $this->treasuryChallanNo;
    }

    /**
     * @param string $treasuryChallanNo
     */
    public function setTreasuryChallanNo(string $treasuryChallanNo)
    {
        $this->treasuryChallanNo = $treasuryChallanNo;
    }

    /**
     * @return string
     */
    public function getAccountCode(): ? string
    {
        return $this->accountCode;
    }

    /**
     * @param string $accountCode
     */
    public function setAccountCode(string $accountCode)
    {
        $this->accountCode = $accountCode;
    }

    /**
     * @return Setting
     */
    public function getTreasuaryCode()
    {
        return $this->treasuaryCode;
    }

    /**
     * @param Setting $treasuaryCode
     */
    public function setTreasuaryCode(Setting $treasuaryCode)
    {
        $this->treasuaryCode = $treasuaryCode;
    }

    /**
     * @return string
     */
    public function getIssuePerson()
    {
        return $this->issuePerson;
    }

    /**
     * @param string $issuePerson
     */
    public function setIssuePerson(string $issuePerson)
    {
        $this->issuePerson = $issuePerson;
    }

    /**
     * @return string
     */
    public function getIssueDesignation()
    {
        return $this->issueDesignation;
    }

    /**
     * @param string $issueDesignation
     */
    public function setIssueDesignation(string $issueDesignation)
    {
        $this->issueDesignation = $issueDesignation;
    }

    /**
     * @return string
     */
    public function getIssueAddress()
    {
        return $this->issueAddress;
    }

    /**
     * @param string $issueAddress
     */
    public function setIssueAddress(string $issueAddress)
    {
        $this->issueAddress = $issueAddress;
    }

    /**
     * @return Location
     */
    public function getDistrict()
    {
        return $this->district;
    }

    /**
     * @param Location $district
     */
    public function setDistrict($district)
    {
        $this->district = $district;
    }

    /**
     * @return string
     */
    public function getAddress(): ? string
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress(string $address)
    {
        $this->address = $address;
    }



}

