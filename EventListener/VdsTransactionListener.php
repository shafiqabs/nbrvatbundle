<?php

namespace Terminalbd\NbrvatBundle\EventListener;


use Doctrine\ORM\Event\LifecycleEventArgs;
use Terminalbd\NbrvatBundle\Entity\VdsTransaction;


class VdsTransactionListener
{
    public function prePersist(LifecycleEventArgs $args)
    {
        $this->createCode($args);
    }

    public function createCode(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        // perhaps you only want to act on some "Purchase" entity
        if ($entity instanceof VdsTransaction) {
            $lastCode = $this->getLastCode($args,$entity);
            $entity->setCode($lastCode+1);
            $barcode = $this->getStrPad($entity->getCode());
            $inv = $entity->getVdsCertificate()->getInvoice()."/".$barcode;
            $entity->setInvoice($inv);
        }
    }



    /**
     * @param LifecycleEventArgs $args
     * @param $entity
     * @return int|mixed
     */
    public function getLastCode(LifecycleEventArgs $args , VdsTransaction $entity)
    {

        $entityManager = $args->getEntityManager();
        $qb = $entityManager->getRepository('TerminalbdNbrvatBundle:VdsTransaction')->createQueryBuilder('s');

            $qb
                ->select('MAX(s.code)')
                ->where('s.config = :config')->setParameter('config', $entity->getConfig()->getId())
                ->andWhere('s.vdsCertificate = :vds')->setParameter('vds', $entity->getVdsCertificate()->getId());
            $lastCode = $qb->getQuery()->getSingleScalarResult();

        if (empty($lastCode)) {
            return 0;
        }
        return $lastCode;
    }

    /**
     * @param @entity
     */

    private  function getStrPad($lastCode)
    {
        $data = str_pad($lastCode,3, '0', STR_PAD_LEFT);
        return $data;
    }
}