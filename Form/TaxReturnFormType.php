<?php
namespace Terminalbd\NbrvatBundle\Form;

use App\Entity\Admin\Bank;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\AccountingBundle\Entity\AccountBank;
use Terminalbd\NbrvatBundle\Entity\BankTreasury;
use Terminalbd\NbrvatBundle\Entity\Setting;
use Terminalbd\NbrvatBundle\Entity\TaxReturn;

/**
 * Defines the form used to create and manipulate blog posts.
 *
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class TaxReturnFormType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder


            ->add('returnType', EntityType::class, array(
                'required'    => false,
                'expanded'      =>false,
                'multiple'      =>false,
                'class' => Setting::class,
                'placeholder' => 'Choose a type of return',
                'choice_label' => 'name',
                'help' => '[If Selected “No” Please Fill Only the relevant
Part]',
                'attr'=>array('class'=>'span12 m-wrap action'),
                'query_builder' => function(EntityRepository $er){
                    return $er->createQueryBuilder('e')
                        ->join("e.settingType","st")
                        ->where("st.slug ='return-type'")
                        ->orderBy('e.name', 'ASC');
                },
            ))
            ->add('taxPeriod', TextType::class, [
                'attr' => ['autofocus' => false, 'class' => 'action text-left dateCalendar col-md-10'],
                'required' => true
            ])
            ->add('refundVatPart11', TextType::class, [
                'attr' => ['autofocus' => false, 'class' => 'col-md-10 text-left action','placeholder'=>'Refund VAT'],
                'required' => false
            ])
            ->add('refundSdPart11', TextType::class, [
                'attr' => ['autofocus' => false, 'class' => 'col-md-10 text-left action','placeholder'=>'Refund SD'],
                'required' => false
            ])
            ->add('submissionDate', DateType::class, [
                'attr' => [
                    'autofocus' => false,
                    'class'=>'action',
                ],
                'required' => true,
                'widget' => 'single_text',
                'html5' => true,
            ])

            ->add('activitiesTaxPeriod',CheckboxType::class,[
                'required' => false,
                'label' => 'Any activities in this Tax Period?',
                'help' => '[If Selected “No” Please Fill Only the relevant
Part]',
                'attr' => [
                    'class' => 'checkboxToggle',
                    'data-toggle' => "toggle",
                    'data-style' => "slow",
                    'data-offstyle' => "warning",
                    'data-onstyle'=> "info",
                    'data-on' => "Yes",
                    'data-off'=> "No"
                ],
            ])
            ->add('signaturePerson', TextType::class, [
                'attr' => ['autofocus' => false, 'class' => 'action text-left'],
                'required' => true
            ])
            ->add('signatureDesignation', TextType::class, [
                'attr' => ['autofocus' => false, 'class' => 'action text-left'],
                'required' => true
            ])
            ->add('signatureMobile', TextType::class, [
                'attr' => ['autofocus' => false, 'class' => 'text-left'],
                'required' => true
            ])
            ->add('signatureEmail', TextType::class, [
                'attr' => ['autofocus' => false, 'class' => 'text-left'],
                'required' => true
            ])
            ->add('signatureNid', TextType::class, [
                'attr' => ['autofocus' => false, 'class' => 'text-left'],
                'required' => true
            ])
        ;

    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => TaxReturn::class,
        ]);
    }
}
