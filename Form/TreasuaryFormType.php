<?php
namespace Terminalbd\NbrvatBundle\Form;

use App\Entity\Admin\Bank;
use App\Entity\Admin\Location;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\AccountingBundle\Entity\AccountBank;
use Terminalbd\NbrvatBundle\Entity\BankTreasury;
use Terminalbd\NbrvatBundle\Entity\Setting;

/**
 * Defines the form used to create and manipulate blog posts.
 *
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class TreasuaryFormType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder

            ->add('treasuryChallanNo', TextType::class, [
                'attr' => ['autofocus' => true],
                'required' => true
            ])
            ->add('treasuaryCode', EntityType::class, array(
                'required'    => true,
                'class' => Setting::class,
                'placeholder' => 'Choose a Bank Treasury Account Code',
                'choice_label' => 'treasuryAccountCode',
                'attr'=>array('class'=>'span12 m-wrap'),
                'query_builder' => function(EntityRepository $er){
                    return $er->createQueryBuilder('e')
                        ->join("e.settingType","st")
                        ->where("st.slug ='part-9'")
                        ->orderBy('e.name', 'ASC');
                },
            ))

            ->add('district', EntityType::class, array(
                'required'    => true,
                'class' => Location::class,
                'placeholder' => 'Choose a Bank District',
                'choice_label' => 'name',
                'attr'=>['class'=>'select2'],
                'query_builder' => function(EntityRepository $er){
                    return $er->createQueryBuilder('e')
                        ->where("e.level = 2")
                        ->orderBy('e.name', 'ASC');
                },
            ))

            ->add('issuePerson', TextType::class, [
                'attr' => ['autofocus' => true,'placeholder' => 'Issue person',],
                'required' => true
            ])

            ->add('issueDesignation', TextType::class, [
                'attr' => ['autofocus' => true,'placeholder' => 'Issue designation',],
                'required' => false
            ])

            ->add('issueAddress', TextType::class, [
                'attr' => ['autofocus' => true,'placeholder' => 'Issue person address',],
                'required' => true
            ])

             ->add('issuePerson', TextType::class, [
                'attr' => ['autofocus' => true,'placeholder' => 'Issue person',],
                'required' => true
            ])

             ->add('bankBranch', TextType::class, [
                'attr' => ['autofocus' => true],
                'required' => true
            ])

             ->add('bankBranch', TextType::class, [
                'attr' => ['autofocus' => true],
                'required' => true
            ])

            ->add('amount', NumberType::class, [
                'attr' => ['autofocus' => true,'class'=>'amount'],
                'required' => true,
                'empty_data' => '0'
            ])

            ->add('address', TextType::class, [
                'attr' => ['autofocus' => true],
                'required' => false
            ])

            ->add('bank', EntityType::class, [
                'class' => Bank::class,
                'required' => true,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('e')
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'select2'],
                'choice_label' => 'name',
                'placeholder' => 'Choose a bank nam',
            ])
            ->add('issueDate', DateType::class, [
                'attr' => [
                    'autofocus' => false,
                    'class'=>'action',
                ],
                'required' => true,
                'widget' => 'single_text',
                'html5' => true,
            ])
            ->add('status',CheckboxType::class,[
                'required' => false,
                'attr' => [
                    'class' => 'checkboxToggle',
                    'data-toggle' => "toggle",
                    'data-style' => "slow",
                    'data-offstyle' => "warning",
                    'data-onstyle'=> "info",
                    'data-on' => "Enabled",
                    'data-off'=> "Disabled"
                ],
            ])
        ;

    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => BankTreasury::class,
        ]);
    }
}
