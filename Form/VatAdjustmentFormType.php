<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Md Shafiqul Islam <shafiqabs@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\NbrvatBundle\Form;

use App\Entity\Admin\SettingType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\NbrvatBundle\Entity\NetTaxPayable;
use Terminalbd\NbrvatBundle\Entity\Setting;
use Terminalbd\NbrvatBundle\Entity\VatAdjustment;


/**
 * Defines the form used to create and manipulate blog posts.
 *
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class VatAdjustmentFormType extends AbstractType
{


    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('remark', TextareaType::class, [
                'attr' => ['autofocus' => true,'class'=>'textarea'],
                'label' => '',
                'required' => false,
            ])
            ->add('amount', NumberType::class, [
                'attr' => ['autofocus' => true],
                'label' => '',
                'required' => true,
            ])
            ->add('adjument', EntityType::class, [
                'class' => Setting::class,
                'required' => true,
                'group_by'  => 'settingType.name',
                'choice_label' => 'name',
                'choice_translation_domain' => true,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('e')
                        ->join('e.settingType','app')
                        ->where('e.status = 1')
                        ->andWhere("app.slug IN ('increasing-adjutment','decreasing-adjutment')")
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'span12'],
                'placeholder' => 'Choose a Adjustment Incresing/Decresing',
            ])

        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => VatAdjustment::class,
        ]);
    }
}
