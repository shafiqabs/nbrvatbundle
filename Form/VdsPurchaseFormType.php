<?php
namespace Terminalbd\NbrvatBundle\Form;

use App\Entity\Admin\Bank;
use App\Entity\Admin\Location;
use App\Entity\Application\Nbrvat;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\AccountingBundle\Entity\AccountBank;
use Terminalbd\NbrvatBundle\Entity\BankTreasury;
use Terminalbd\NbrvatBundle\Entity\Setting;
use Terminalbd\NbrvatBundle\Entity\VdsCertificate;
use Terminalbd\NbrvatBundle\Entity\VdsTransaction;

/**
 * Defines the form used to create and manipulate blog posts.
 *
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class VdsPurchaseFormType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $nbrvat =  $options['nbrvat']->getId();

        $builder

            ->add('vdsCertificate', EntityType::class, array(
                'required'    => true,
                'class' => VdsCertificate::class,
                'placeholder' => 'Choose a invoice no',
                'choice_label' => 'vdsPurchaseInvoice',
                'attr' => array('class'=>'vdsCertificate select2'),
                'query_builder' => function(EntityRepository $er) use($nbrvat){
                    return $er->createQueryBuilder('e')
                        ->join('e.purchase','purchase')
                        ->where("e.config ='{$nbrvat}'")
                        ->andWhere("e.mode ='purchase'")
                        ->andWhere("e.isClose =1")
                        ->orderBy('e.id', 'ASC');
                },
            ))
            ->add('amount', NumberType::class, [
                'attr' => ['autofocus' => true,'class'=>'amount'],
                'required' => true,
                'empty_data' => 0
            ])

            ->add('treasuryChallanNo', TextType::class, [
                'attr' => ['autofocus' => true],
                'required' => false
            ])

            ->add('treasuaryCode', EntityType::class, array(
                'required'    => false,
                'class' => Setting::class,
                'placeholder' => 'Choose a Bank Treasury Account Code',
                'choice_label' => 'treasuryAccountCode',
                'attr'=>array('class'=>'span12 m-wrap'),
                'query_builder' => function(EntityRepository $er){
                    return $er->createQueryBuilder('e')
                        ->join("e.settingType","st")
                        ->where("st.slug ='part-9'")
                        ->orderBy('e.name', 'ASC');
                },
            ))

            ->add('district', EntityType::class, array(
                'required'    => false,
                'class' => Location::class,
                'placeholder' => 'Choose a Bank District',
                'choice_label' => 'name',
                'attr'=>['class'=>'select2'],
                'query_builder' => function(EntityRepository $er){
                    return $er->createQueryBuilder('e')
                        ->where("e.level = 2")
                        ->orderBy('e.name', 'ASC');
                },
            ))

            ->add('issuePerson', TextType::class, [
                'attr' => ['autofocus' => true,'placeholder' => 'Issue person',],
                'required' => false
            ])

            ->add('issueDesignation', TextType::class, [
                'attr' => ['autofocus' => true,'placeholder' => 'Issue designation',],
                'required' => false
            ])

            ->add('issueAddress', TextType::class, [
                'attr' => ['autofocus' => true,'placeholder' => 'Issue person address',],
                'required' => false
            ])

            ->add('issuePerson', TextType::class, [
                'attr' => ['autofocus' => true,'placeholder' => 'Issue person',],
                'required' => false
            ])

            ->add('bankBranch', TextType::class, [
                'attr' => ['autofocus' => true],
                'required' => false
            ])

            ->add('bankBranch', TextType::class, [
                'attr' => ['autofocus' => true],
                'required' => false
            ])


            ->add('address', TextType::class, [
                'attr' => ['autofocus' => true],
                'required' => false
            ])

            ->add('bank', EntityType::class, [
                'class' => Bank::class,
                'required' => false,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('e')
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'select2'],
                'choice_label' => 'name',
                'placeholder' => 'Choose a bank nam',
            ])

            ->add('issueDate', DateType::class, [
                'attr' => [
                    'autofocus' => false,
                    'class'=>'action',
                ],
                'required' => false,
                'widget' => 'single_text',
                'html5' => true,
            ])

            ->add('remark', TextType::class, [
                'attr' => ['autofocus' => true],
                'required' => false
            ]);

    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class'    => VdsTransaction::class,
            'nbrvat'        => Nbrvat::class,
        ]);
    }
}
