<?php

namespace Terminalbd\NbrvatBundle\Repository;
use Appstore\Bundle\InventoryBundle\Entity\InventoryConfig;
use Doctrine\ORM\EntityRepository;
use Terminalbd\InventoryBundle\Entity\Brand;
use Terminalbd\InventoryBundle\Entity\Item;
use Terminalbd\NbrvatBundle\Entity\Setting;
use Terminalbd\NbrvatBundle\Entity\TaxSetup;

/**
 * ItemBrandRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class SettingRepository extends EntityRepository
{
    public function getChildRecords($parent)
    {
        $em = $this->_em;
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.settingType','p');
        $qb->select('e.id as id','e.noteNo as noteNo','e.slug as slug','e.name as name');
        $qb->where('p.app = :app')->setParameter('app',1);
        //   $qb->andWhere('p.slug = :slug')->setParameter('slug',$parent);
        $qb->andWhere('e.status = :status')->setParameter('status',1);
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }

    public function getSettings($arrs = [])
    {
        $em = $this->_em;
        $qb = $em->createQueryBuilder();
        $qb->where('e.app = :app')->setParameter('app', 1);
        // $qb->andWhere('e.slug IN (:slugs)')->setParameter('slugs',$arrs);
        $result  = $qb->getQuery()->getResult();
        return $result;

    }


    public function getPurchaseInputTax($parent,$mode)
    {
        $em = $this->_em;
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.settingType','p');
        $qb->select('e.id as id','e.noteNo as noteNo','e.slug as slug','e.name as name');
        $qb->where('p.slug = :slug')->setParameter('slug',$parent);
        $qb->andWhere('e.mode = :mode')->setParameter('mode',$mode);
        $qb->andWhere('p.app = :app')->setParameter('app',1);
        $qb->orderBy('e.noteNo',"ASC");
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }


    public function getChildObjectRecords($parent)
    {
        $em = $this->_em;
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.settingType','p');
        $qb->where('p.slug = :slug')->setParameter('slug',$parent);
        $qb->andWhere('e.status = :status')->setParameter('status',1);
        $qb->orderBy('e.noteNo','ASC');
        $result = $qb->getQuery()->getResult();
        return $result;
    }


    public function supplyInputTax(Item $item)
    {
        $particular = $item->getMasterItem()->getInputTax()->getNoteNo();
        $data = "";
        if($particular == "14"){
            $data ='<input type="number" class="form-control form-control-lg col-md-12" disabled="disabled" readonly="readonly" value="15"><input type="hidden" id="vatPercent" name="vatPercent"  value="15"><button class="btn btn-warning" type="button">%</button>';
        }elseif($particular == "18"){
            $data ='<input type="text"  id="vatPercent" name="vatPercent"  class="form-control form-control-lg col-md-10"  value="" placeholder="VAT 450,100 etc">
                    <button class="btn btn-warning" type="button">Flat</button>';
        }elseif($particular == "16"){
            $data ='<select class="form-control form-control-lg col-md-12" id="vatPercent" name="vatPercent">
                <option value="0">0%</option>
                <option value="5">%</option>
                <option value="7.5">7.5%</option>
                <option value="10">10%</option>
                <option value="10">15%</option>
                </select> <button class="btn btn-warning" type="button">%</button>';
        }else{
            $data ='<input type="number" class="form-control form-control-lg col-md-11" id="vatPercent" name="vatPercent" max="100"  value="" placeholder="VAT"> <button class="btn btn-warning" type="button">%</button>';
        }

        return $data;
    }

    public function supplyOutTax(Item $item)
    {
        $particular = $item->getMasterItem()->getSupplyOutputTax()->getNoteNo();
        $data = "";
        if($particular == "4"){
            $data ='<input type="number" class="form-control form-control-lg col-md-12" disabled="disabled" readonly="readonly" value="15"><input type="hidden" id="vatPercent" name="vatPercent"  value="15"><button class="btn btn-warning" type="button">%</button>';
        }elseif($particular == "6"){
            $data ='<input type="text"  id="vatPercent" name="vatPercent"  class="form-control form-control-lg col-md-10"  value="" placeholder="VAT 450,100 etc">
                    <button class="btn btn-warning" type="button">Flat</button>';
        }elseif($particular == "7"){
            $data ='<select class="form-control form-control-lg col-md-12" id="vatPercent" name="vatPercent">
                <option value="5">5%</option>
                <option value="7.5">7.5%</option>
                <option value="10">10%</option>
                <option value="15">15%</option>
                </select> <button class="btn btn-warning" type="button">%</button>';
        }else{
            $data ='<input type="number" class="form-control form-control-lg col-md-11" id="vatPercent" name="vatPercent" max="100"  value="" placeholder="VAT"> <button class="btn btn-warning" type="button">%</button>';
        }

        return $data;
    }



}
