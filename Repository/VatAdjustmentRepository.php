<?php

namespace Terminalbd\NbrvatBundle\Repository;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Terminalbd\NbrvatBundle\Entity\TaxReturn;
use Terminalbd\NbrvatBundle\Entity\VatAdjustment;

/**
 * ItemBrandRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class VatAdjustmentRepository extends EntityRepository
{

    public function rowCount($data)
    {

        $qb = $this->createQueryBuilder('e');
        $qb->select('count(e.id) as count');
        $this->handleSearchBetween($qb,$data);
        $count =  $qb->getQuery()->getOneOrNullResult();
        return $count['count'];
    }



    public function findWidthSearch($parameter,$data){


        if (!empty($parameter['orderBy'])) {
            $sortBy = $parameter['orderBy'];
            $order = $parameter['order'];
        }
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.hsCode as hscode','e.name as name','e.customsDuty as cd','e.supplementaryDuty as sd','e.valueAddedTax as vat','e.advanceIncomeTax as ait','e.advanceTradeVat as atv','e.recurringDeposit as rd','e.totalTaxIncidence as tti');
        $qb->where('e.name IS NOT NULL');
        $this->handleSearchBetween($qb,$data);
        $qb->setFirstResult($parameter['offset']);
        $qb->setMaxResults($parameter['limit']);
        if ($parameter['orderBy']){
            $qb->orderBy($sortBy, $order);
        }else{
            $qb->orderBy('e.hscode', 'DESC');
        }
        $result =  $qb->getQuery()->getArrayResult();
        return $result;
    }


    public function getTotalAdjustment(TaxReturn $taxReturn)
    {
        $arrs = array();
        $dataEx = explode('-',$taxReturn->getTaxPeriod());
        $date = strtotime("{$dataEx[1]}-{$dataEx[0]}-01");
        $startDate = date('Y-m-01 00:00:00',$date);
        $endDate = date('Y-m-t 23:59:59',$date);

        $config = $taxReturn->getConfig();

        $qb = $this->createQueryBuilder('e');
        $qb->select('setting.slug as mode','SUM(e.amount) as amount');
        $qb->join('e.adjument','adjument');
        $qb->join('adjument.settingType','setting');
        $qb->where("e.config = :config")->setParameter('config', $config->getId());
        $qb->andWhere('adjument.slug != :note25')->setParameter('note25','increasing-adjutment-payment-not-made-through-banking-channel');
        $qb->andWhere('e.process = :process')->setParameter('process','approved');
        if ($startDate) {
            $qb->andWhere("e.updated >= :startDate")->setParameter('startDate',$startDate);
        }
        if ($endDate) {
            $qb->andWhere("e.updated <= :endDate")->setParameter('endDate',$endDate);
        }
        $qb->groupBy('setting.slug');
        $result =  $qb->getQuery()->getArrayResult();
        foreach ($result as $row):
            $arrs[$row['mode']] = $row;
        endforeach;
        return $arrs;
    }

    public function getAdjustmentParticular(TaxReturn $taxReturn,$mode)
    {
        $arrs = array();
        $dataEx = explode('-',$taxReturn->getTaxPeriod());
        $date = strtotime("{$dataEx[1]}-{$dataEx[0]}-01");
        $startDate = date('Y-m-01 00:00:00',$date);
        $endDate = date('Y-m-t 23:59:59',$date);

        $config = $taxReturn->getConfig();

        $qb = $this->createQueryBuilder('e');
        $qb->select('adjument.name as particular');
        $qb->join('e.adjument','adjument');
        $qb->join('adjument.settingType','setting');
        $qb->where("e.config = :config")->setParameter('config', $config->getId());
        $qb->andWhere('adjument.slug != :note25')->setParameter('note25','increasing-adjutment-payment-not-made-through-banking-channel');
        $qb->andWhere('e.process = :process')->setParameter('process','approved');
        $qb->andWhere('setting.slug = :slug')->setParameter('slug',$mode);
        $qb->groupBy('adjument.name');
        if ($startDate) {
            $qb->andWhere("e.updated >= :startDate")->setParameter('startDate',$startDate);
        }
        if ($endDate) {
            $qb->andWhere("e.updated <= :endDate")->setParameter('endDate',$endDate);
        }
        $result =  $qb->getQuery()->getArrayResult();
        return $result;
    }



    public function getTotalAdjustmentBankingChannel(TaxReturn $taxReturn)
    {
        $arrs = array();
        $dataEx = explode('-',$taxReturn->getTaxPeriod());
        $date = strtotime("{$dataEx[1]}-{$dataEx[0]}-01");
        $startDate = date('Y-m-01 00:00:00',$date);
        $endDate = date('Y-m-t 23:59:59',$date);

        $config = $taxReturn->getConfig();
        $qb = $this->createQueryBuilder('e');
        $qb->select('adjument.name','SUM(e.amount) as amount');
        $qb->join('e.adjument','adjument');
        $qb->where("e.config = :config")->setParameter('config', $config->getId());
        $qb->andWhere('adjument.slug = :slug')->setParameter('slug','increasing-adjutment-payment-not-made-through-banking-channel');
        $qb->andWhere('e.process = :process')->setParameter('process','approved');
        if ($startDate) {
            $qb->andWhere("e.updated >= :startDate")->setParameter('startDate',$startDate);
        }
        if ($endDate) {
            $qb->andWhere("e.updated <= :endDate")->setParameter('endDate',$endDate);
        }
        $result =  $qb->getQuery()->getOneOrNullResult();
        return $result;
    }

    public function getVatRefundExport(TaxReturn $taxReturn,$outputRebateTax)
    {
        $em = $this->_em;
        $dataEx = explode('-',$taxReturn->getTaxPeriod());
        $date = strtotime("{$dataEx[1]}-{$dataEx[0]}-01");
        $endDate = date('Y-m-t 11:30:30',$date);
        $dateTime = new \DateTime($endDate);

        $setting = $em->getRepository('TerminalbdNbrvatBundle:Setting')->findOneBy(array('slug' => 'decreasing-adjutment-vat-refund-aganist-export'));
        $exist = $this->findOneBy(array('adjument' => $setting, 'taxreturn' => $taxReturn));
        if($exist){
            $entity = $exist;
        }else{
            $entity = new VatAdjustment();
        }
        $amount = isset($outputRebateTax['vat']) ? $outputRebateTax['vat'] :0;
        $entity->setConfig($taxReturn->getConfig());
        $entity->setAdjument($setting);
        $entity->setTaxreturn($taxReturn);
        $entity->setAmount($amount);
        $entity->setProcess('approved');
        $entity->setCreatedBy($taxReturn->getCreatedBy());
        $entity->setCheckedBy($taxReturn->getCreatedBy());
        $entity->setApprovedBy($taxReturn->getCreatedBy());
        $entity->setCreated($dateTime);
        $entity->setUpdated($dateTime);
        $em->persist($entity);
        $em->flush();

    }

    public function getVatRebateForSales(TaxReturn $taxReturn,$outputRebateTax)
    {
        $em = $this->_em;
        $dataEx = explode('-',$taxReturn->getTaxPeriod());
        $date = strtotime("{$dataEx[1]}-{$dataEx[0]}-01");
        $endDate = date('Y-m-t 11:30:30',$date);
        $dateTime = new \DateTime($endDate);

        $setting = $em->getRepository('TerminalbdNbrvatBundle:Setting')->findOneBy(array('slug' => 'increasing-adjutment-rebate-refund-aganist-sales'));
        $exist = $this->findOneBy(array('adjument'=>$setting, 'taxreturn'=>$taxReturn));
        if($exist){
            $entity = $exist;
        }else{
            $entity = new VatAdjustment();
        }
        $amount = isset($outputRebateTax['refundVat']) ? $outputRebateTax['refundVat'] :0;
        $entity->setConfig($taxReturn->getConfig());
        $entity->setAdjument($setting);

        $entity->setTaxreturn($taxReturn);
        $entity->setAmount($amount);
        $entity->setProcess('approved');
        $entity->setCreatedBy($taxReturn->getCreatedBy());
        $entity->setCheckedBy($taxReturn->getCreatedBy());
        $entity->setApprovedBy($taxReturn->getCreatedBy());
        $entity->setCreated($dateTime);
        $entity->setUpdated($dateTime);
        $em->persist($entity);
        $em->flush();
    }



}
